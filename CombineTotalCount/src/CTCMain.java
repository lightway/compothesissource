import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map.Entry;


public class CTCMain {

	private String file1="C:\\Users\\User\\SkyDrive\\Documents\\MA\\2\\Results\\wordcount\\wordcount\\callgraph_pycharm_dateierstellen_261214.txt";
	private String file2="C:\\Users\\User\\SkyDrive\\Documents\\MA\\2\\Results\\wordcount\\wordcount\\callgraph_argouml_dateierstellen_261214.txt";
	private String file3="C:\\Users\\User\\SkyDrive\\Documents\\MA\\2\\Results\\wordcount\\wordcount\\callgraph_joeffice_dateierstellen_261214.txt";
	private String outputfile="C:\\Users\\User\\SkyDrive\\Documents\\MA\\2\\Results\\wordcount\\wordcount\\combinedtotal_dateierstellen.txt";
	private String cleanedsuffix="_cleaned.txt";
	private BufferedReader reader1;
	private BufferedReader reader2;
	private BufferedReader reader3;
	private BufferedReader cleanreader1;
	private BufferedReader cleanreader2;
	private BufferedReader cleanreader3;
	private PrintWriter writeout;
	private PrintWriter cleanwriteout;
	private HashMap<String, Integer> results;
	private HashMap<String, Integer> cleanedresults;
	public static void main(String[] args) {
		CTCMain ctc = new CTCMain();
		try {
			ctc.calculateTotal(ctc.getReader1(), ctc.getResults());
			ctc.calculateTotal(ctc.getReader2(), ctc.getResults());
			ctc.calculateTotal(ctc.getReader3(), ctc.getResults());
			ctc.writeResult(ctc.getResults(), ctc.getWriteout());
			ctc.calculateTotal(ctc.getCleanreader1(), ctc.getCleanedresults());
			ctc.calculateTotal(ctc.getCleanreader2(), ctc.getCleanedresults());
			ctc.calculateTotal(ctc.getCleanreader3(), ctc.getCleanedresults());
			ctc.writeResult(ctc.getCleanedresults(), ctc.getCleanwriteout());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public CTCMain(){
		results = new HashMap<String, Integer>();
		cleanedresults = new HashMap<String, Integer>();
		try {
			reader1=new BufferedReader(new FileReader(file1));
			reader2=new BufferedReader(new FileReader(file2));
			reader3=new BufferedReader(new FileReader(file3));
			cleanreader1 = new BufferedReader(new FileReader(file1+cleanedsuffix));
			cleanreader2 = new BufferedReader(new FileReader(file2+cleanedsuffix));
			cleanreader3 = new BufferedReader(new FileReader(file3+cleanedsuffix));
			writeout=new PrintWriter(outputfile);
			cleanwriteout = new PrintWriter(outputfile+cleanedsuffix);
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void calculateTotal(BufferedReader readme, HashMap<String,  Integer> workresult) throws IOException{
		String line="";
		String name;
		int value;
		String[] split;
		while ((line = readme.readLine()) != null) {
			split=line.split(":");
			name=split[0];
			if (!name.equalsIgnoreCase("Total count of tokens")){
				value = Integer.parseInt(split[1]);
				addOrAppend(workresult, name, value);
			}
		}
		readme.close();
	}
		
	private void addOrAppend(HashMap<String, Integer> workresult, String name, int value){
		if (workresult.containsKey(name)){
			int newvalue = workresult.get(name)+value;
			workresult.put(name, newvalue);
		} else {
			workresult.put(name, value);
		}
	}
	private void writeResult(HashMap<String, Integer> resultmap, PrintWriter out){
		for (Entry<String,Integer> e: resultmap.entrySet()){
			out.println(e.getKey()+":"+Integer.toString(e.getValue()));
		}
		out.flush();
		out.close();
	}

	public BufferedReader getReader1() {
		return reader1;
	}

	public void setReader1(BufferedReader reader1) {
		this.reader1 = reader1;
	}

	public BufferedReader getReader2() {
		return reader2;
	}

	public void setReader2(BufferedReader reader2) {
		this.reader2 = reader2;
	}

	public BufferedReader getReader3() {
		return reader3;
	}

	public void setReader3(BufferedReader reader3) {
		this.reader3 = reader3;
	}

	public BufferedReader getCleanreader1() {
		return cleanreader1;
	}

	public void setCleanreader1(BufferedReader cleanreader1) {
		this.cleanreader1 = cleanreader1;
	}

	public BufferedReader getCleanreader2() {
		return cleanreader2;
	}

	public void setCleanreader2(BufferedReader cleanreader2) {
		this.cleanreader2 = cleanreader2;
	}

	public BufferedReader getCleanreader3() {
		return cleanreader3;
	}

	public void setCleanreader3(BufferedReader cleanreader3) {
		this.cleanreader3 = cleanreader3;
	}

	public PrintWriter getWriteout() {
		return writeout;
	}

	public void setWriteout(PrintWriter writeout) {
		this.writeout = writeout;
	}

	public PrintWriter getCleanwriteout() {
		return cleanwriteout;
	}

	public void setCleanwriteout(PrintWriter cleanwriteout) {
		this.cleanwriteout = cleanwriteout;
	}

	public HashMap<String, Integer> getResults() {
		return results;
	}

	public void setResults(HashMap<String, Integer> results) {
		this.results = results;
	}

	public HashMap<String, Integer> getCleanedresults() {
		return cleanedresults;
	}

	public void setCleanedresults(HashMap<String, Integer> cleanedresults) {
		this.cleanedresults = cleanedresults;
	}
}
