package CallTypes;

import java.util.ArrayList;

public class CallElement {

	private String packageName;
	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public ArrayList<CallElement> getCallees() {
		return callees;
	}

	public void setCallees(ArrayList<CallElement> callees) {
		this.callees = callees;
	}

	public String getThreadName() {
		return threadName;
	}

	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}

	private String className;
	private ArrayList<CallElement> callees;
	private String threadName;
	private ArrayList<CallElement> callers;
	
	public ArrayList<CallElement> getCallers() {
		return callers;
	}

	public void setCallers(ArrayList<CallElement> callers) {
		this.callers = callers;
	}

	public CallElement(String packageName, String className, ArrayList<CallElement> callees, String threadName, ArrayList<CallElement> callers) {
		this.packageName = packageName;
		this.className = className;
		if (callees==null) {
			this.callees = new ArrayList<CallElement>();
		}
		else {
			this.callees = callees;
		}
		if (callers==null) {
			this.callers = new ArrayList<CallElement>();
		}
		else {
			this.callers = callers;
		}
		this.threadName = threadName;
	}
	public void addCallee(CallElement temp){
		this.callees.add(temp);
	}
	public void addCaller(CallElement tempi) {
		this.callers.add(tempi);
	}
	
	@Override
	public String toString() {
		String output = this.packageName+" "+this.getClassName()+" calls:";
		System.out.println(this.callees.size());
		for (CallElement e : this.callees) {			
			output+=" "+e.getPackageName()+"."+e.getClassName();
		}
		output+=" -- and was called from: ";
		for (CallElement f : this.callers) {
			output+=" "+f.getPackageName()+"."+f.getClassName();
		}
		return output;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((callees == null) ? 0 : callees.hashCode());
		result = prime * result
				+ ((className == null) ? 0 : className.hashCode());
		result = prime * result
				+ ((packageName == null) ? 0 : packageName.hashCode());
		result = prime * result
				+ ((threadName == null) ? 0 : threadName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CallElement other = (CallElement) obj;
		if (callees == null) {
			if (other.callees != null)
				return false;
		} else if (!callees.equals(other.callees))
			return false;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (packageName == null) {
			if (other.packageName != null)
				return false;
		} else if (!packageName.equals(other.packageName))
			return false;
		if (threadName == null) {
			if (other.threadName != null)
				return false;
		} else if (!threadName.equals(other.threadName))
			return false;
		return true;
	}
	
}
