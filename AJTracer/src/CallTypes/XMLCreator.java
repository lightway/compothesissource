package CallTypes;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 




import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLCreator {
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder;
	Document doc;
	
	public XMLCreator() {
		try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
        doc = dBuilder.newDocument();
        Element mainRootElement = doc.createElement("Calls");
        doc.appendChild(mainRootElement);
	}
	
	public void addNodeUnderNode(String whereToAdd, String whatToAdd) { //fix, only one level??
		//Create a Node under another Node
		Element tempToAdd = doc.createElement(whatToAdd);
		NodeList myList = doc.getChildNodes();
		Element tempSource = null;
		for (int i=0; i<myList.getLength(); i++) {
			if (((Element)myList.item(i)).getNodeName().equalsIgnoreCase(whereToAdd)) {
				tempSource = (Element)myList.item(i);
			}
		}
		if (tempSource !=null){
			tempSource.appendChild(tempToAdd);
		}
		
	}
	public void addElementToNode(String whereToAdd, String text) {
		//add a "line" to a existing node
		Node tempNode = doc.createElement(text);
		NodeList myList = doc.getElementsByTagName("*");
		Element tempSource = null;
		for (int i=0; i<myList.getLength(); i++) {
			if (((Element)myList.item(i)).getNodeName().equalsIgnoreCase(whereToAdd)) {
				tempSource = (Element)myList.item(i);
			}
		}
		if (tempSource != null){
			tempSource.appendChild(tempNode);
		}
	}
	public Element getElementByName(String name) {
		//Find an Element by it's name
		return null;
	}
	public boolean checkExistElement(String elename) {
		NodeList myList = doc.getElementsByTagName("*");
		boolean foundit = false;
		for (int i=0; i<myList.getLength(); i++) {
			if (((Element)myList.item(i)).getNodeName().equalsIgnoreCase(elename)) {
				foundit=true;
			}
		}
		return foundit;
	}
	
	public void writeToFile() {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
		DOMSource source = new DOMSource(doc);
		StreamResult result =  new StreamResult(new File("C:\\Temp\\testing.xml"));
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}

	}
}
