package CallTypes;

public class SimpleCallElement {

	private String callPackage;
	private String callClass;
	private String calledPackage;
	private String calledClass;
	private int sequence;
	private long timestamp;
	private String threadName;
	
	public SimpleCallElement(String callPackage, String callClass, String calledPackage, String calledClass, int sequence, long timestamp, String threadName) {
		this.callPackage=callPackage;
		this.callClass = callClass;
		this.calledPackage=calledPackage;
		this.calledClass = calledClass;
		this.sequence=sequence;
		this.timestamp = timestamp;
		this.threadName = threadName;
	}

	public String getCallPackage() {
		return callPackage;
	}

	public void setCallPackage(String callPackage) {
		this.callPackage = callPackage;
	}

	public String getCallClass() {
		return callClass;
	}

	public void setCallClass(String callClass) {
		this.callClass = callClass;
	}

	public String getCalledPackage() {
		return calledPackage;
	}

	public void setCalledPackage(String calledPackage) {
		this.calledPackage = calledPackage;
	}

	public String getCalledClass() {
		return calledClass;
	}

	public void setCalledClass(String calledClass) {
		this.calledClass = calledClass;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getThreadName() {
		return threadName;
	}

	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}
}
