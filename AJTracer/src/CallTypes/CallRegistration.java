package CallTypes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.Timer;
import java.util.TimerTask;


public class CallRegistration {
    private static final CallRegistration OBJ = new CallRegistration();  //final for removing the syncrhonized in method
	private static Object mutex= new Object();
	private static Timer timer;
    private List<String> theList = Collections.synchronizedList(new ArrayList<String>());
    private List<String> theMethodList = Collections.synchronizedList(new ArrayList<String>());
    private ConcurrentMap<String, Integer> theMap = new ConcurrentHashMap<String, Integer>();
    private ConcurrentMap<String, Integer> theMethodMap = new ConcurrentHashMap<String, Integer>();
    private ConcurrentMap<String, String> theMethodSequenceMap = new ConcurrentHashMap<String, String>();
    private ConcurrentMap<String, Integer> theThreadMap = new ConcurrentHashMap<String, Integer>();
    private ConcurrentMap<String, Integer> theSequenceCOMap = new ConcurrentHashMap<String, Integer>();
    private int sequence;
    private int commitCounter;
    private int resultCounter;
    private int resultID;
    private int resultMID;
    private Integer sID;
    private Integer dID;
    private Integer sMID;
    private Integer dMID;
    private boolean skipTracing;
    private java.util.Date date= new java.util.Date();
    private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	private ResultSet resultSetID = null;
	private ResultSet resultSetMethods = null;
	private ResultSet resultSetMethodID = null;
	private ResultSet resultSetProperty = null;
	String dpackageName;
	String dclassName;
	String dMethod;
	String sMethod;
	private PreparedStatement iobjstat;
	private PreparedStatement iconnstat;
	private PreparedStatement sobjstat;
	private PreparedStatement sallobj;
	private PreparedStatement smethod;
	private PreparedStatement imethod;
	private PreparedStatement sallmethods;
	private PreparedStatement getIsSkip;
	String selectIsSkip = "select propvalue from properties where propname='skipTracing'";
	String selectObject = "select id from callobject where packageName=? and className=?";
	String insertObjectQuery = " insert into callobject (packageName, className) values (?, ?);";
	String insertConnectionQuery = " insert into calls(caller, callee, callermethod, calleemethod, sequence, timestamp, threadname) values(?, ?, ?, ?, ?, ?, ?);" ;
	String selectAllObjects = "select * from callobject";
	String selectMethodQuery = "select methodid from comethod where callobject=? and methodname=? and sequence=?";
	String insertMethodQuery = "insert into comethod(callobject, methodname, sequence) values(?, ?, ?)";
	String selectAllMethodsQuery = "select * from comethod";
	String pkgcl;
	String fullMethodString;
	String sourceFullMethodString;
	int methodObjectID;
	private int resultMCounter;
	private int sourceID;
	private Stack<String> methodStack;
	private int initialCO;
	private String tempSource;
	private Integer sourceSequence;
	private String sourceThreadName;
	private String storePackage;
	private String storeClass;
	private String storeMethod;
	private String storeThread;
	private int initMethodId;
	private String propertyValue;
	
    private CallRegistration() {
    	sequence = 1;
    	commitCounter=0;
    	skipTracing = false;
    	timer = new Timer();
    	try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
    	try {
    		
    		connect = DriverManager
	          .getConnection("jdbc:mysql://192.168.56.102:3306/callgraph?"
	              + "user=root&password=abc123");
			connect.setAutoCommit(false);
			iobjstat=connect.prepareStatement(insertObjectQuery);
			sobjstat = connect.prepareStatement(selectObject);
			iconnstat=connect.prepareStatement(insertConnectionQuery);
			sallobj=connect.prepareStatement(selectAllObjects);
			imethod=connect.prepareStatement(insertMethodQuery);
			smethod=connect.prepareStatement(selectMethodQuery);
			sallmethods=connect.prepareStatement(selectAllMethodsQuery);
			getIsSkip=connect.prepareStatement(selectIsSkip);
			resultSet = sallobj.executeQuery();
			resultSetMethods = sallmethods.executeQuery();
			while (resultSet.next()){
				theMap.put(resultSet.getString(2)+":"+resultSet.getString(3), resultSet.getInt(1) );
				theList.add(resultSet.getString(2)+":"+resultSet.getString(3));
			}
			Iterator it = theMap.entrySet().iterator();
			while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
			}
			
			while (resultSetMethods.next()){
				theMethodMap.put(Integer.toString(resultSetMethods.getInt(2))+":"+resultSetMethods.getInt(4)+":"+resultSetMethods.getString(3), resultSetMethods.getInt(1) );
				theList.add(Integer.toString(resultSet.getInt(2))+":"+resultSet.getString(3));
			}
			Iterator itm = theMethodMap.entrySet().iterator();
			while (itm.hasNext()) {
		        Map.Entry mpairs = (Map.Entry)itm.next();
			}
			skipTracing=this.isSkipTracing();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	methodStack = new Stack<String>();
    	initialCO=doInitalCOEntry();
    	doInitialMethodEntry();
    	tempSource="";
        System.out.println("Objekt gebildet...");
        if (skipTracing==true) {
        	System.out.println("Warning, tracing is disabled in properties!");
        } else {
        	System.out.println("Starting application with AspectJ Tracing to DB turned on!");
        }
    } 
    
    public synchronized void addElementSynchronized(String callPackage, String callClass, String calledPackage, String calledClass, String calledMethod, int sequencex, long timestamp, String threadName) {
    	this.addElement(callPackage, callClass, calledPackage, calledClass, calledMethod, sequencex, timestamp, threadName);
    }
    
    public synchronized void addElement(String callPackage, String callClass, String calledPackage, String calledClass, String calledMethod, int sequencex, long timestamp, String threadName) {
    	
    	
    	if (theThreadMap.containsKey(threadName)){
    		sourceSequence = theThreadMap.get(threadName);
    	} else {
    		theThreadMap.put(threadName, new Integer(1));
    		System.out.println("Pushing initial from ThreadMap in thread "+threadName);
    		sourceSequence=1;
    		theSequenceCOMap.put(Integer.toString(sourceSequence)+"_"+threadName, initialCO);
    	}
    	sequence=sourceSequence+1;

    	if(!sourceThreadName.equals(threadName)){
    		if (theThreadMap.containsKey(sourceThreadName)) {
    			int sourcetempbefore = sourceSequence;
    			sourceSequence = theThreadMap.get(sourceThreadName);
    			System.out.println("Different threads detected, altering source sequence from "+Integer.toString(sourcetempbefore)+" to "+Integer.toString(sourceSequence));
    		}
    	} else {
    		if (sourceSequence+1 != sequence){
    			System.out.println("ERROR!!!! gap between sequences in same threas, program will be terminated to keep integrity!");
    			System.out.println("The Thread is "+threadName+" the source Thread was "+sourceThreadName+" the sequence is "+Integer.toString(sequence)+" the sourceSequence is "+Integer.toString(sourceSequence)+" called was: "+calledPackage+":"+calledClass+"::"+calledMethod);
    			System.exit(1);
    		}
    	}
    	
    	if (sourceSequence <0 || sequence <0) {
    		System.out.println("ERROR!!!! sourceSequence or sequence is <0, program will be terminated to keep integrity!");
    		System.out.println("The Thread is "+threadName+" the source Thread was "+sourceThreadName+" the sequence is "+Integer.toString(sequence)+" the sourceSequence is "+Integer.toString(sourceSequence)+" called was: "+calledPackage+":"+calledClass+"::"+calledMethod);
    		System.exit(1);
    	}
    	dpackageName=calledPackage;
    	dclassName=calledClass;
    	dMethod=calledMethod;

    	if (dpackageName==null || dpackageName.isEmpty()) {
    		System.out.println("dpackageName is null!");
    		dpackageName="None";
    	}
    	if (dclassName==null || dclassName.isEmpty()){
    		System.out.println("dclassName is null!");
    		dclassName="None";
    	}
    	if (dMethod == null || dMethod.isEmpty()){
    		System.out.println("dMethod is null or empty!");
    		dMethod="None";
    	}

    	try {
    		
			pkgcl=dpackageName+":"+dclassName;

				if (!theList.contains(pkgcl)) {
					if (iobjstat != null){
						iobjstat.clearParameters();
					}
					iobjstat.setString(1, dpackageName);
					iobjstat.setString(2, dclassName);
					iobjstat.execute();
					connect.commit();
					theList.add(pkgcl);
					if (sobjstat !=null){
						sobjstat.clearParameters();
					}
					sobjstat.setString(1, dpackageName);
					sobjstat.setString(2, dclassName);
					resultSetID = sobjstat.executeQuery();
					resultCounter =0;
					while (resultSetID.next()) {
						resultID = resultSetID.getInt(1);
						theMap.put(pkgcl, resultID);
						resultCounter++;
					}
					resultSetID.close();
					if (resultCounter >1 || resultCounter == 0){
						System.out.println("Warning, possible Database inconsistence detected");
						if (resultCounter == 0) {
							resultID=initialCO;
						}
					}

				}
				else {
					//System.out.println("The Object is already in the list");
				}
				
				if (sourceFullMethodString == null || !theMethodSequenceMap.containsKey(Integer.toString(sourceSequence)+"_"+sourceThreadName)) {
					System.out.println("Warning, Sequence error for "+Integer.toString(sequence)+" with full DMethod x"+fullMethodString);
					sourceFullMethodString = initialCO+":0:"+"UNKNOWN"; 
				} else {
					sourceFullMethodString = theMethodSequenceMap.get(Integer.toString(sourceSequence)+"_"+sourceThreadName);
					for (Entry<String, Integer> entry : theMap.entrySet()) { //Performance ??
			            if (entry.getValue() == Integer.parseInt(sourceFullMethodString.split(":")[0])) {
			            	//System.out.println("I parse the sheriff "+sourceFullMethodString.split(":")[0]);
			            	tempSource=entry.getKey();
			            }
			            else {
			            	//System.out.println("I failed the sheriff "+sourceFullMethodString.split(":")[0]);
			            }
			        }

					theMethodSequenceMap.remove(Integer.toString(sourceSequence)+"_"+threadName); //to not get another memory leak
				}
				//System.out.println(theMap.get(dpackageName+":"+dclassName));
				if (theMap == null) {
					System.out.println("The Map is null");
				}
				if (dpackageName == null || dclassName == null) {
					System.out.println("The package or classname is null");
				}
				methodObjectID=theMap.get(dpackageName+":"+dclassName);
				fullMethodString = Integer.toString(methodObjectID)+":"+dMethod;
				theMethodSequenceMap.put(Integer.toString(sequence)+"_"+threadName, fullMethodString);
				if (!theMethodList.contains(fullMethodString)) {
					if (imethod != null) {
						imethod.clearParameters();
					}
					imethod.setInt(1, methodObjectID);
					imethod.setString(2, dMethod);
					imethod.setInt(3, sequence);
					imethod.execute();
					connect.commit();
					theMethodList.add(fullMethodString);
					if (smethod != null){
						smethod.clearParameters();
					}
					smethod.setInt(1, methodObjectID);
					smethod.setString(2, dMethod);
					smethod.setInt(3, sequence);
					resultSetMethodID = smethod.executeQuery();
					resultMCounter = 0;
					while(resultSetMethodID.next()) {
						resultMID=resultSetMethodID.getInt(1);
						theMethodMap.put(fullMethodString, resultMID);
						resultMCounter++;
					}
					resultSetMethodID.close();
					if (resultMCounter >1 || resultMCounter == 0){
						System.out.println("Warning, possible Database inconsistence in table comethods detected, the result of the counter is "+Integer.toString(resultMCounter)+", I wanted to put in there: "+fullMethodString);
						if (resultMCounter ==0 ) {
							resultMID = initMethodId;
						}
					}
				}
				else {
					//System.out.println("Method already in the list: "+fullMethodString);
				}
				
				if (iconnstat !=null){
					iconnstat.clearParameters();
				} else {
					System.out.println("Iconnstat is null!");
				}
				dMID = theMethodMap.get(theMap.get(dpackageName+":"+dclassName)+":"+dMethod);
				sMID = theMethodMap.get(sourceFullMethodString);

				if (dMID == null) {
					System.out.println("The dMID is null!");
					dMID = initMethodId;
				}
				if (sMID == null) {
					System.out.println("The sMID is null!");
					sMID=initMethodId;
				}
				sID=theSequenceCOMap.get(Integer.toString(sourceSequence)+"_"+sourceThreadName);
				theSequenceCOMap.remove(Integer.toString(sourceSequence)+"_"+sourceThreadName); //to not get a memory leak
				dID = theMap.get(dpackageName+":"+dclassName);
				
				//System.out.println("The values are "+Integer.toString(sourceSequence)+", "+Integer.toString(sequence));
				if (sID == null) {
					System.out.println("The sID is null for "+Integer.toString(sourceSequence)+"_"+sourceThreadName+" with sequence "+Integer.toString(sequence));
					sID=initialCO;
				}
				if (dID == null) {
					System.out.println("The dID is null for "+dpackageName+"_"+dclassName+" with sequence "+Integer.toString(sequence));
					dID=initialCO;
				}
				theSequenceCOMap.put(Integer.toString(sequence)+"_"+threadName, dID);
				if (iconnstat !=null && sID !=null && dID != null && sMID !=null && dMID !=null) {
					iconnstat.setInt(1, sID);
					iconnstat.setInt(2, dID);
					iconnstat.setInt(3, sMID);
					iconnstat.setInt(4, dMID);
					iconnstat.setInt(5, sequence);
					Long ts = date.getTime();
					String sts = ts.toString();
					iconnstat.setString(6, sts);
					iconnstat.setString(7, threadName);
					iconnstat.execute(); 
					theThreadMap.put(threadName, sequence);
					//System.out.println("I pushed to "+threadName+" the sequence "+Integer.toString(sequence));
				}
				else {
					System.out.println("There is something null when trying to insert a call!! "+"the threadname is"+threadName);
				}
				this.sourceThreadName=threadName;
		} catch (SQLException e) {
			//System.out.println("spackageName="+spackageName+", sclassName="+sclassName+", dpackageName="+dpackageName+", dclassName="+dclassName+";");
			System.out.println("Exception in SQL!");
			System.out.println("I tried to execute with following parameters: sID="+Integer.toString(sID)+" dID="+Integer.toString(dID)+" sMID="+Integer.toString(dMID)+" sequence="+Integer.toString(sequence)+" threadName="+threadName);
			e.printStackTrace();
		}

    	if (commitCounter <=1000) {  
    		commitCounter ++;
    	} else {
    		try {
				connect.commit();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    		commitCounter=0;
    	}
    }
    
    public void doFinalCommit() {
    	try {
			connect.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    private int doInitalCOEntry() {
    	int tempid=0;
    	int tempcount=0;
    	try {
    	if (iobjstat != null){
			iobjstat.clearParameters();
		}
		
			iobjstat.setString(1, "UNKNOWN");
			iobjstat.setString(2, "UNKNOWN");
			iobjstat.execute();
			connect.commit();
			theList.add("UNKNOWN:UNKNOWN");
			this.storePackage="UNKNOWN";
			this.storeClass="UNKNOWN";
			this.storeThread="main";
			if (sobjstat !=null){
				sobjstat.clearParameters();
			}
			sobjstat.setString(1, "UNKNOWN");
			sobjstat.setString(2, "UNKNOWN");
			resultSetID = sobjstat.executeQuery();
			while (resultSetID.next()) {
				tempid = resultSetID.getInt(1);
				theMap.put("UNKNOWN:UNKNOWN", tempid);
				tempcount++;
			}
			if (tempcount >1 || tempcount == 0){
				System.out.println("Warning, possible Database inconsistence detected for initial commit");
			}
			return tempid;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tempid;
		
    }
    
    private int doInitialMethodEntry() {
    	int tempMCounter=0;
    	int tempMID=0;
    	this.sourceThreadName="main"; //should be the case all the time
    	this.sourceSequence=1;
    	try {
    		if (!theMethodList.contains(fullMethodString)) {
				if (imethod != null) {
					imethod.clearParameters();
				}
				imethod.setInt(1, initialCO);
				imethod.setString(2, "UNKNOWN");
				imethod.setInt(3, 0);
				imethod.execute();
				connect.commit();
				theMethodList.add("0"+":UNKNOWN");
				this.storeMethod="UNKNOWN";
				if (smethod != null){
					smethod.clearParameters();
				}
				smethod.setInt(1, initialCO);
				smethod.setString(2, "UNKNOWN");
				smethod.setInt(3, 0);
				resultSetMethodID = smethod.executeQuery();
				tempMCounter = 0;
				while(resultSetMethodID.next()) {
					tempMID=resultSetMethodID.getInt(1);
					initMethodId=tempMID;
					theMethodMap.put("0:"+"UNKNOWN", tempMID);
					tempMCounter++;
				}
				if (tempMCounter >1 || tempMCounter == 0){
					System.out.println("Warning, possible Database inconsistence in table comethods detected in init");
				}
			}
    		
    	} catch (SQLException e) {
			e.printStackTrace();
		}
    	return tempMID;
    }
    public static CallRegistration getInstance() {
        return OBJ;
    }

	public synchronized String getSourceThreadName() {
		return sourceThreadName;
	}

	public synchronized String getStorePackage() {
		return storePackage;
	}

	public synchronized void setStorePackage(String storePackage) {
		this.storePackage = storePackage;
	}

	public synchronized String getStoreClass() {
		return storeClass;
	}

	public synchronized void setStoreClass(String storeClass) {
		this.storeClass = storeClass;
	}

	public synchronized String getStoreMethod() {
		return storeMethod;
	}

	public synchronized void setStoreMethod(String storeMethod) {
		this.storeMethod = storeMethod;
	}

	public synchronized String getStoreThread() {
		return storeThread;
	}

	public synchronized void setStoreThread(String storeThread) {
		this.storeThread = storeThread;
	}

	public synchronized boolean isSkipTracing() {
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				
			}
			
		},0,1000);
		try {
			resultSetProperty = getIsSkip.executeQuery();
			while(resultSetProperty.next()) {
				propertyValue=resultSetProperty.getString(1);
			}
			resultSetProperty.close();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		if (propertyValue.equalsIgnoreCase("true")) {
			return true;
		} else {
			return false;
		}
	}
 }
