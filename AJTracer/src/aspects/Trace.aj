package aspects;
 
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import CallTypes.*;

import org.aspectj.lang.Signature;

aspect Trace{
 
	CallRegistration register = CallRegistration.getInstance();
	private static String oldPackage="UNKNOWN";
	private static String oldClass="UNKNOWN";
	private static String oldMethod="UNKOWN";
    pointcut traceMethods() : (execution(* *(..))&& !cflow(within(Trace) && !cflow(within(SimpleCallElement)) && !cflow(within(CallRegistration)))); //change to call
    pointcut callMain() : execution(public static void *(..));
 
    before(): traceMethods(){
    	String threadName = Thread.currentThread().getName().replace(" ", "");
        Signature sig = thisJoinPointStaticPart.getSignature();
        String dmethod = sig.getName();
        if (dmethod == null || dmethod.isEmpty()) {
        	dmethod="Unknown";
        }
        
        String[] fullDest = sig.getDeclaringTypeName().split("\\.");
        String dclassName = "";
        if (fullDest.length >0) {
        	dclassName = fullDest[fullDest.length-1];
        	dclassName=dclassName.replace('$', '_');
        }
        String dpackageName = "";
        if (fullDest.length >1) {
        	dpackageName = fullDest[0];
        	for (int j=1; j<fullDest.length-1; j++) {
        		dpackageName+="."+fullDest[j];
        	}
        }
        
        
        if (!dpackageName.equals(oldPackage) && !dclassName.equals(oldClass) && !dmethod.equals(oldMethod)) {
        	if(!register.isSkipTracing()){
        		register.addElement("nix", "nix", dpackageName, dclassName, dmethod, 0 ,0, threadName);
        	}
        }
        oldPackage=dpackageName;
        oldClass=dclassName;
        oldMethod=dmethod;

    }
    after(): callMain(){
    	register.doFinalCommit();
    	
    }
 
}
