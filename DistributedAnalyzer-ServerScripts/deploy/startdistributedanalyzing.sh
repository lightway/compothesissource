#!/bin/bash
whichclient=$1
while read line
do
 server=`echo $line |cut -d':' -f1`
 numproc=`echo $line |cut -d':' -f2`
 mem=`echo $line |cut -d':' -f3`
 ssh -t -t root@$server /root/runclient.sh $numproc $mem $whichclient </dev/null
done < /root/deploy/clientlist.txt

