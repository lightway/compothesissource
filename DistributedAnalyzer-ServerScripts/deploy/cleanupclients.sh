#!/bin/bash
filename=$1
myhostname=`hostname`
while read line
do
 server=`echo $line |cut -d':' -f1`
 echo $server
 if [ "$server" != "$myhostname" ]
 then
  ssh -t -t root@$server killall java </dev/null
 fi
done < /root/deploy/clientlist.txt 
