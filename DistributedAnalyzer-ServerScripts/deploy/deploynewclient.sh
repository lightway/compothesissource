#!/bin/bash
filename=$1
myhostname=`hostname`
while read line
do
 server=`echo $line |cut -d':' -f1`
 echo $server
 if [ "$server" != "$myhostname" ]
 then
  scp $filename root@$server:/root/
 fi
done < /root/deploy/clientlist.txt 
