#!/bin/bash
max=$1
mem=$2
whichclient=$3
echo "$max;$mem;$whichclient" >/tmp/clientparas.txt
rm -rf /tmp/logout.txtMain*
for (( c=1; c<=$max; c++ ))
do
echo "Starting $c on `hostname` of total $max"
 screen -S client$c -L -d -m /opt/java/bin/java -Xmx$mem -Xms2048m -jar /root/$whichclient 
 sleep 10
done
