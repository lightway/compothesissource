package types;

import interfaces.IWorkPackage;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class WorkPackage extends UnicastRemoteObject implements IWorkPackage{
	private boolean isForward;
	private int startsequence1;
	private int startsequence2;
	private ArrayList<String> left;
	private ArrayList<String> right;
	private ArrayList<String> result;
	private long timestamp;
	private int workerID;
	private boolean alreadyRetried;
	private boolean resultDone;
	private int packageID;
	private boolean markforresent;
	
	public WorkPackage(boolean isForward, int seq1, int seq2, ArrayList<String> left, ArrayList<String> right, int packageID) throws RemoteException{
		this.isForward=isForward;
		this.startsequence1=seq1;
		this.startsequence2=seq2;
		this.left=left;
		this.right=right;
		this.packageID=packageID;
		alreadyRetried=false;
		resultDone=false;
		markforresent=false;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#isForward()
	 */
	@Override
	public boolean isForward() throws RemoteException {
		return isForward;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setForward(boolean)
	 */
	@Override
	public void setForward(boolean isForward) throws RemoteException {
		this.isForward = isForward;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#getStartsequence1()
	 */
	@Override
	public int getStartsequence1() throws RemoteException{
		return startsequence1;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setStartsequence1(int)
	 */
	@Override
	public void setStartsequence1(int startsequence1) throws RemoteException{
		this.startsequence1 = startsequence1;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#getStartsequence2()
	 */
	@Override
	public int getStartsequence2() throws RemoteException{
		return startsequence2;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setStartsequence2(int)
	 */
	@Override
	public void setStartsequence2(int startsequence2) throws RemoteException{
		this.startsequence2 = startsequence2;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#getLeft()
	 */
	@Override
	public ArrayList<String> getLeft() throws RemoteException{
		return left;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setLeft(java.util.ArrayList)
	 */
	@Override
	public void setLeft(ArrayList<String> left) throws RemoteException{
		this.left = left;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#getRight()
	 */
	@Override
	public ArrayList<String> getRight() throws RemoteException{
		return right;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setRight(java.util.ArrayList)
	 */
	@Override
	public void setRight(ArrayList<String> right) throws RemoteException{
		this.right = right;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#getTimestamp()
	 */
	@Override
	public long getTimestamp() throws RemoteException{
		return timestamp;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setTimestamp(long)
	 */
	@Override
	public void setTimestamp(long timestamp) throws RemoteException{
		this.timestamp = timestamp;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#getWorkerID()
	 */
	@Override
	public int getWorkerID() throws RemoteException{
		return workerID;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setWorkerID(int)
	 */
	@Override
	public void setWorkerID(int workerID) throws RemoteException{
		this.workerID = workerID;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#getResult()
	 */
	@Override
	public ArrayList<String> getResult() throws RemoteException{
		return result;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setResult(java.util.ArrayList)
	 */
	@Override
	public void setResult(ArrayList<String> result) throws RemoteException{
		this.result = result;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#isAlreadyRetried()
	 */
	@Override
	public boolean isAlreadyRetried() throws RemoteException{
		return alreadyRetried;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setAlreadyRetried(boolean)
	 */
	@Override
	public void setAlreadyRetried(boolean alreadyRetried) throws RemoteException{
		this.alreadyRetried = alreadyRetried;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#isResultDone()
	 */
	@Override
	public boolean isResultDone() throws RemoteException{
		return resultDone;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setResultDone(boolean)
	 */
	@Override
	public void setResultDone(boolean resultDone) throws RemoteException{
		this.resultDone = resultDone;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#getPackageID()
	 */
	@Override
	public int getPackageID() throws RemoteException{
		return packageID;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setPackageID(int)
	 */
	@Override
	public void setPackageID(int packageID) throws RemoteException{
		this.packageID = packageID;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#isMarkforresent()
	 */
	@Override
	public boolean isMarkforresent() throws RemoteException{
		return markforresent;
	}

	/* (non-Javadoc)
	 * @see types.IWorkPackage#setMarkforresent(boolean)
	 */
	@Override
	public void setMarkforresent(boolean markforresent) throws RemoteException{
		this.markforresent = markforresent;
	}

}
