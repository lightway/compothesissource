package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import utilities.PropertyConfig;

public class ServerMain {

	private static String db1;
	private static String db2;
	private static SendPackageImpl spi;
	private static PropertyConfig config;
	public static void main(String[] args) {
		config = new PropertyConfig();
		db1=config.getDBName1();
		db2=config.getDBName2();
		try {
			spi = new SendPackageImpl(config.getGap());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		spi.fillLists(db1, db2);
		ServerMain main = new ServerMain();
		main.startServer();

	}
	
	private void startServer(){
		System.setProperty("java.rmi.server.hostname", config.getServer()); 
        try {
            Registry registry = LocateRegistry.createRegistry(config.getServerPort());
            registry.rebind("AnalyzeServer", spi);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("analyzer server is ready");
    }

}
