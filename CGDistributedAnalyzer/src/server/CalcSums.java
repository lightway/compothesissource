package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import utilities.CommonLogger;
import utilities.PropertyConfig;

public class CalcSums {
	private File input;
	private FileReader readfile;
	private BufferedReader buffread;
	private PrintWriter buffwrite1;
	private PrintWriter buffwrite2;
	private Map<String, Integer> fullMatch;
	private Map<String, Integer> singleTokenMatch;
	private static PropertyConfig config = new PropertyConfig();
	private CommonLogger clogger = new CommonLogger(config.getLogFileName()+this.getClass().getSimpleName());
	
	public CalcSums() {
		fullMatch = new HashMap<String, Integer>();
		singleTokenMatch = new HashMap<String, Integer>();
		input = new File(config.getOutputFileName());
		try {
			buffwrite1 = new PrintWriter(config.getResultFileFull());
			buffwrite2 = new PrintWriter(config.getResultFileToken());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void findFullLineMatch() throws IOException {
		//Read file line by line and count full matches (src+dst)
		String line;
		String[] tempsplit;
		Integer tempint=0;
		String stringi;
		readfile = new FileReader(input);
		buffread = new BufferedReader(readfile);
		while ((line=buffread.readLine()) !=null){
			tempsplit=line.split(":");
			if (tempsplit.length == 6){
			stringi=tempsplit[2]+":"+tempsplit[3]+":"+tempsplit[4]+":"+tempsplit[5];
				if (fullMatch.containsKey(stringi)) {
					tempint = fullMatch.get(stringi);
					tempint++;
					fullMatch.put(stringi, tempint);
				} else {
					fullMatch.put(stringi, 1);
				}
			} else {
				clogger.systemOutLogger("Invalid line detected, the content is "+line);
			}
		}
		for(Entry<String, Integer> ks : fullMatch.entrySet()){
			buffwrite1.println(ks.getKey()+":"+Integer.toString(ks.getValue())+"\n");
		}
		buffwrite1.flush();
		buffwrite1.close();
		
	}
	
	public void findSingleTokenMatch() throws IOException {
		//Read file line by line and count occurence of sinlge tokens, does not matter if left or rigt
		String line;
		String[] tempsplit;
		Integer tempint=0;
		readfile = new FileReader(input);
		buffread = new BufferedReader(readfile);
		while ((line=buffread.readLine()) !=null){
			tempsplit=line.split(":");
			if (tempsplit.length == 6){
				if (singleTokenMatch.containsKey(tempsplit[2])) {
					tempint = singleTokenMatch.get(tempsplit[2]);
					tempint++;
					singleTokenMatch.put(tempsplit[2], tempint);
				} else {
					singleTokenMatch.put(tempsplit[2], 1);
				}
				if (singleTokenMatch.containsKey(tempsplit[3])) {
					tempint = singleTokenMatch.get(tempsplit[3]);
					tempint++;
					singleTokenMatch.put(tempsplit[3], tempint);
				} else {
					singleTokenMatch.put(tempsplit[3], 1);
				}
			} else {
				clogger.systemOutLogger("Invalid line in SingleToken detected, the content is "+line);
			}
		}
		for(Entry<String, Integer> ks : singleTokenMatch.entrySet()){
			buffwrite2.println(ks.getKey()+":"+Integer.toString(ks.getValue())+"\n");
		}
		buffwrite2.flush();
		buffwrite2.close();
	}

}
