package server;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import server.CalcSums;
import tests.TestFileReader;
import cdaverification.DatabaseChecker;
import utilities.CommonLogger;
import utilities.PropertyConfig;
import types.WorkPackage;
import interfaces.ISendPackage;
import interfaces.IWorkPackage;

public class SendPackageImpl extends UnicastRemoteObject implements ISendPackage{
	private static ArrayList<String> fullList1;
	private static ArrayList<String> fullList2;
	private ArrayList<String> partialList1;
	private ArrayList<String> partialList2;
	private ArrayList<String> emptyList;
	private int segmentation;
	private int gap;
	private int forwardCounter;
	private int reverseCounter;
	private int nextClientID;
	private boolean isready;
	private int list1Size;
	private int list2Size;
	private long currentmillis;
	private long maxtimeout;
	private static PropertyConfig config;
	private String[] pkgfilter;
	private int pkgfiltercounter;
	private CommonLogger clogger;
	private PrintWriter debugwriter;
	private int nextforwardNo;
	private int nextreverseNo;
	private ConcurrentHashMap<Integer, WorkPackage> forwardPackages;
	private ConcurrentHashMap<Integer, WorkPackage> reversePackages;
	private ArrayList<String> augmentedFullList1;
	private ArrayList<String> augmentedFullList2;
	private ArrayList<Long> takenTimes;
	private long timetaken;
	
	protected SendPackageImpl(int gap) throws RemoteException {
		this.gap=gap;
		config = new PropertyConfig();
		clogger = new CommonLogger(config.getLogFileName());
		pkgfilter=config.getPackageFilter();
		maxtimeout=config.getPackageTimeout();
		nextforwardNo=0;
		nextreverseNo=0;
		nextClientID=-1;
		if(gap<=1){
			nextreverseNo=1; //Else we never finish the operation if no reverse calculation is set;
		}
		forwardPackages = new ConcurrentHashMap<Integer, WorkPackage>();
		reversePackages = new ConcurrentHashMap<Integer, WorkPackage>();
		emptyList= new ArrayList<String>();
		augmentedFullList1=new ArrayList<String>();
		augmentedFullList2=new ArrayList<String>();
		takenTimes = new ArrayList<Long>();
		try {
			debugwriter = new PrintWriter(config.getOutputFileName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public IWorkPackage getWork(int clientID) throws RemoteException {
		currentmillis = System.currentTimeMillis();
		if (nextforwardNo<=forwardCounter){
			IWorkPackage temp = forwardPackages.get(nextforwardNo);
			temp.setWorkerID(clientID);
			temp.setTimestamp(currentmillis);
			clogger.systemOutLogger("I gave the forward package with the ID:"+Integer.toString(nextforwardNo)+" the the client "+Integer.toString(clientID));
			nextforwardNo++;	
			return temp;
		} else {
			if (nextreverseNo<=reverseCounter && gap>1){
				IWorkPackage temp = reversePackages.get(nextreverseNo);
				temp.setWorkerID(clientID);
				temp.setTimestamp(currentmillis);
				clogger.systemOutLogger("I gave the reverse package with the ID:"+Integer.toString(nextreverseNo)+" the the client "+Integer.toString(clientID));
				nextreverseNo++;
				return temp;
			}
		}
		if (nextforwardNo>forwardCounter && nextreverseNo>reverseCounter){
			//check if we miss something
			boolean switchi=true;
			boolean wearedone=true;
			for (Entry<Integer, WorkPackage> e : forwardPackages.entrySet()){
				if (!e.getValue().isResultDone()){
					switchi=false;
					if (currentmillis-e.getValue().getTimestamp()>=maxtimeout){
						if (e.getValue().isAlreadyRetried()){
							switchi=true;
						} else {
							e.getValue().setMarkforresent(true);
						}
					}
				}
				if (wearedone==true && switchi==false){
					wearedone=false;
				}
			}
			if(gap>1){
				for (Entry<Integer, WorkPackage> e : reversePackages.entrySet()){
					if (!e.getValue().isResultDone()){
						switchi=false;
						if (currentmillis-e.getValue().getTimestamp()>=maxtimeout){
							if (e.getValue().isAlreadyRetried()){
								switchi=true;
							} else {
								e.getValue().setMarkforresent(true);
							}
						}
					}
					if (wearedone==true && switchi==false){
						wearedone=false;
					}
				}
			}
			if (wearedone){
				//terminate program and  calculate results
				programFinish();
				
			} else {
				for (Entry<Integer, WorkPackage> e : forwardPackages.entrySet()){
					if (e.getValue().isMarkforresent() && !e.getValue().isAlreadyRetried() && !e.getValue().isResultDone()){
						e.getValue().setAlreadyRetried(true);
						e.getValue().setWorkerID(clientID);
						e.getValue().setTimestamp(currentmillis);
						clogger.systemOutLogger("I resent the forward package with the ID "+e.getValue().getPackageID()+" to the Client: "+Integer.toString(clientID));
						return e.getValue();
					}
				}
				if(gap>1){
					for (Entry<Integer, WorkPackage> e : reversePackages.entrySet()){
						if (e.getValue().isMarkforresent() && !e.getValue().isAlreadyRetried() && !e.getValue().isResultDone()){
							e.getValue().setAlreadyRetried(true);
							e.getValue().setWorkerID(clientID);
							e.getValue().setTimestamp(currentmillis);
							clogger.systemOutLogger("I resent the reverse package with the ID "+e.getValue().getPackageID()+" to the Client: "+Integer.toString(clientID));
							return e.getValue();
						}
					}
				}
			}
		}
		return null;
		
	}

	@Override
	public boolean returnResult(IWorkPackage result) throws RemoteException {
		//check if this package is already resent, if yes throw away
		if (result.isForward()){
			if (forwardPackages.get(result.getPackageID()) !=null && !forwardPackages.get(result.getPackageID()).isResultDone()){
				forwardPackages.get(result.getPackageID()).setResultDone(true);
				for (String s : result.getResult()){
					debugwriter.println(s);
				}
				debugwriter.flush();
				clogger.systemOutLogger("Received package No. "+Integer.toString(result.getPackageID())+"with a size of "+Integer.toString(result.getResult().size()));
				timetaken = System.currentTimeMillis()-forwardPackages.get(result.getPackageID()).getTimestamp();
				takenTimes.add(timetaken);
				recalcTimeout();
				if(forwardPackages.remove(result.getPackageID())==null){
					//save memory?
					clogger.systemOutLogger("Error in removing returning Package ID "+Integer.toString(result.getPackageID())+"from forwardList after processing it!");
				}
				result.getResult().clear();
				result=null;
			} else {
				clogger.systemOutLogger("I throw away forward result of "+Integer.toString(result.getPackageID()));
				result.getResult().clear();
				result=null;
			}
		} else {
			//Returning reverse operation
			if (reversePackages.get(result.getPackageID()) !=null && !reversePackages.get(result.getPackageID()).isResultDone()){
				reversePackages.get(result.getPackageID()).setResultDone(true);
				for (String s : result.getResult()){
					debugwriter.println(s);
				}
				debugwriter.flush();
				clogger.systemOutLogger("Received reverse package No. "+Integer.toString(result.getPackageID())+"with a size of "+Integer.toString(result.getResult().size()));
				timetaken = System.currentTimeMillis()-reversePackages.get(result.getPackageID()).getTimestamp();
				takenTimes.add(timetaken);
				recalcTimeout();
				if(reversePackages.remove(result.getPackageID())==null){
					//save memory?
					clogger.systemOutLogger("Error in removing returning Package ID "+Integer.toString(result.getPackageID())+"from reverseList after processing it!");
				}
				result.getResult().clear();
				result=null;
			} else {
				clogger.systemOutLogger("I throw away reverse result of "+Integer.toString(result.getPackageID()));
				result.getResult().clear();
				result=null;
			}
		}
		result=null;
		return false;
	}
	
	public void fillLists(String db1, String db2) {		
		if (config.getIsTestRun()) { //Check if we just have a test run, if yes read from test files
			clogger.systemOutLogger("Started test run on Server with file1="+config.getTestFile1()+" and file2="+config.getTestFile2());
			TestFileReader tf = new TestFileReader();
			fullList1=tf.getTestList1();
			fullList2=tf.getTestList2();
		} else {
			DatabaseChecker dbchecker = new DatabaseChecker(db1);
			DatabaseChecker dbchecker2 = new DatabaseChecker(db2);
			fullList1 = dbchecker.getFullListWithMethods();
			fullList2 = dbchecker2.getFullListWithMethods();
		}
		if (pkgfilter.length>0){
			fullList1=checkPackageFilter1();
			fullList2=checkPackageFilter2();
			clogger.systemOutLogger("I filtered out "+Integer.toString(pkgfiltercounter)+" entries from both list due to package filter setting!");
		}
		list1Size = fullList1.size();
		list2Size = fullList2.size();
		//If we have no reverse operation, we need to swap to the bigger list as the segmented one
		if (list1Size<list2Size && gap<=1){
			ArrayList<String> tempSwap = new ArrayList<String>();
			tempSwap=fullList1;
			fullList1=fullList2;
			fullList2=tempSwap;
			list1Size=fullList1.size();
			list2Size=fullList2.size();
			clogger.systemOutLogger("I swapped the FullLists in favor for the bigger one because there is no gap and no reverse!");
		}
		int relevantListSize;
		relevantListSize=list1Size;
		int relevantSegmentationSize=0;
		if (list1Size>list2Size) {
			relevantSegmentationSize = list1Size;
		} else {
			relevantSegmentationSize = list2Size;
		}

		segmentation=config.getMaxSegmentation();
		clogger.systemOutLogger("Segmentation set to "+Integer.toString(segmentation));
		generateAugmentedFullList1();
		generateAugmentedFullList2();
		try {
			generateForwardWorkPackages();
			if (gap>1){
				generateReverseWorkPackages();
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		clogger.systemOutLogger("Loaded DBs into memory!");
		for (Entry<Integer, WorkPackage> e : forwardPackages.entrySet()){
			try {
				System.out.println(e.getValue().getLeft().size());
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
		isready=true;
	}
	private ArrayList<String> checkPackageFilter1(){
		String[] temp;
		ArrayList<String> tempFullList1 = new ArrayList<String>();
		boolean filtered=false;
		for (String t : fullList1) {
			temp=t.split(">>");
			for (String block : Arrays.asList(pkgfilter)) {
				if (temp[0].startsWith(block) || temp[1].startsWith(block)){
					pkgfiltercounter++;
					filtered=true;
				} else {
					filtered=false;				
				}
			}
			if (!filtered){
				tempFullList1.add(t);
			}
		}
		return tempFullList1;
	}
	
	private ArrayList<String> checkPackageFilter2(){
		String[] temp;
		ArrayList<String> tempFullList2 = new ArrayList<String>();
		boolean filtered2=false;
		for (String t : fullList2) {
			temp=t.split(">>");
			for (String block : Arrays.asList(pkgfilter)) {
				if (temp[0].startsWith(block) || temp[1].startsWith(block)){
					pkgfiltercounter++;
					filtered2=true;
				} else {
					filtered2=false;
					
				}
			}
			if (!filtered2){
				tempFullList2.add(t);
			}
		}
		return tempFullList2;
	}

	@Override
	public ArrayList<String> getFullList1() throws RemoteException {
		return augmentedFullList1;
	}

	@Override
	public ArrayList<String> getFullList2() throws RemoteException {
		return augmentedFullList2;
	}
	
	public void generateAugmentedFullList1() {
		int seqi =0;
		String istr="";
		for (String i: fullList1){
			istr=Integer.toString(seqi)+">>"+i;
			augmentedFullList1.add(istr);
			istr="";
			seqi++;
		}
		clogger.systemOutLogger("Generated Augmented FullList1");
	}
	
	public void generateAugmentedFullList2() {
		int seqi =0;
		String istr="";
		for (String i: fullList2){
			istr=Integer.toString(seqi)+">>"+i;
			augmentedFullList2.add(istr);
			istr="";
			seqi++;
		}
		clogger.systemOutLogger("Generated Augmented FullList2");
	}
	
	private void generateForwardWorkPackages() throws RemoteException{
		int packageCounter=-1;
		int currentcounter=0;
		int oldcounter=0;
		String addString1;
		while(currentcounter<fullList1.size()){
			packageCounter++;
			ArrayList<String> partialList = new ArrayList<String>();
			oldcounter=currentcounter;
			for (int i = currentcounter; i<currentcounter+segmentation;i++) {
				if (i<fullList1.size()){
					addString1 = Integer.toString(i)+">>"+fullList1.get(i);
					partialList.add(addString1);
				}
			}
			WorkPackage temp = new WorkPackage(true, oldcounter, 0, partialList, emptyList, packageCounter);
			forwardPackages.put(packageCounter, temp);
			clogger.systemOutLogger("Put into forwardPackages: "+Integer.toString(packageCounter)+" with start sequence:"+Integer.toString(oldcounter));			
			currentcounter=currentcounter+segmentation;
		}
		forwardCounter=packageCounter;
	}
	
	private void generateReverseWorkPackages() throws RemoteException{
		int packageCounter=-1;
		int currentcounter=0;
		int oldcounter=0;
		String addString1;
		while(currentcounter<fullList2.size()){
			packageCounter++;
			ArrayList<String> partialList = new ArrayList<String>();
			oldcounter=currentcounter;
			for (int i = currentcounter; i<currentcounter+segmentation;i++) {
				if (i<fullList2.size()){
					addString1 = Integer.toString(i)+">>"+fullList2.get(i);
					partialList.add(addString1);
				}
			}
			WorkPackage temp = new WorkPackage(false, oldcounter, 0, partialList, emptyList, packageCounter);
			reversePackages.put(packageCounter, temp);
			clogger.systemOutLogger("Put into reversePackages: "+Integer.toString(packageCounter)+" with start sequence:"+Integer.toString(oldcounter));			
			currentcounter=currentcounter+segmentation;
		}
		reverseCounter=packageCounter;
	}

	@Override
	public IWorkPackage getMissingPackage(int clientID) throws RemoteException {
		for (Entry<Integer, WorkPackage> re : forwardPackages.entrySet()) {
			if (re.getValue().isMarkforresent() && re.getValue().isAlreadyRetried()){
				re.getValue().setAlreadyRetried(true);
				return re.getValue();
			}
		}
		for (Entry<Integer, WorkPackage> re : reversePackages.entrySet()) {
			if (re.getValue().isMarkforresent() && re.getValue().isAlreadyRetried()){
				re.getValue().setAlreadyRetried(true);
				return re.getValue();
			}
		}
		return null;
	}

	@Override
	public int getMyClientIDOnce() throws RemoteException {
		nextClientID++;
		return nextClientID;
	}
	private void programFinish(){
		if (isready){
			isready=false;
			clogger.systemOutLogger("Everything done on server, bye bye everyone!");
			debugwriter.close();
			if (config.doResultsCalc()) {
				CalcSums cs = new CalcSums();
				try {
					cs.findFullLineMatch();
					cs.findSingleTokenMatch();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			clogger.systemOutLogger("Something called programFinish when program was already finished!");
		}
	}

	@Override
	public boolean isServerReady() throws RemoteException {
		return isready;
	}

	@Override
	public int getGap() throws RemoteException {
		return gap;
	}
	private void recalcTimeout(){
		//calculate the new maxtimeout with values from the real taken times and add 10% as buffer
		long sum=0;
		for (Long x : takenTimes){
			sum=sum+x;
		}
		maxtimeout=sum/takenTimes.size();
		maxtimeout=maxtimeout+(maxtimeout/10);
		if(maxtimeout <config.getPackageTimeout()){
			maxtimeout=config.getPackageTimeout();
		}
	}
}
