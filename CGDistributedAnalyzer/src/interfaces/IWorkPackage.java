package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface IWorkPackage extends Remote{

	public abstract boolean isForward() throws RemoteException;

	public abstract void setForward(boolean isForward) throws RemoteException;

	public abstract int getStartsequence1() throws RemoteException;

	public abstract void setStartsequence1(int startsequence1) throws RemoteException;

	public abstract int getStartsequence2() throws RemoteException;

	public abstract void setStartsequence2(int startsequence2) throws RemoteException;

	public abstract ArrayList<String> getLeft() throws RemoteException;

	public abstract void setLeft(ArrayList<String> left) throws RemoteException;

	public abstract ArrayList<String> getRight() throws RemoteException;

	public abstract void setRight(ArrayList<String> right) throws RemoteException;

	public abstract long getTimestamp() throws RemoteException;

	public abstract void setTimestamp(long timestamp) throws RemoteException;

	public abstract int getWorkerID() throws RemoteException;

	public abstract void setWorkerID(int workerID) throws RemoteException;

	public abstract ArrayList<String> getResult() throws RemoteException;

	public abstract void setResult(ArrayList<String> result) throws RemoteException;

	public abstract boolean isAlreadyRetried() throws RemoteException;

	public abstract void setAlreadyRetried(boolean alreadyRetried) throws RemoteException;

	public abstract boolean isResultDone() throws RemoteException;

	public abstract void setResultDone(boolean resultDone) throws RemoteException;

	public abstract int getPackageID() throws RemoteException;

	public abstract void setPackageID(int packageID) throws RemoteException;

	public abstract boolean isMarkforresent() throws RemoteException;

	public abstract void setMarkforresent(boolean markforresent) throws RemoteException;

}