package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ISendPackage extends Remote{

	public IWorkPackage getWork(int clientID) throws RemoteException;
	public boolean returnResult(IWorkPackage result) throws RemoteException;
	public ArrayList<String> getFullList1() throws RemoteException;
	public ArrayList<String> getFullList2() throws RemoteException;
	public IWorkPackage getMissingPackage(int clientID) throws RemoteException;
	public int getMyClientIDOnce() throws RemoteException;
	public boolean isServerReady() throws RemoteException;
	public int getGap() throws RemoteException;
}
