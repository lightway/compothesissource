package client;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import edu.mit.jwi.IRAMDictionary;
import edu.mit.jwi.RAMDictionary;
import edu.mit.jwi.data.ILoadPolicy;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import utilities.CommonLogger;
import interfaces.ISendPackage;
import interfaces.IWorkPackage;
import utilities.PropertyConfig;

public class ClientMain {
	private PropertyConfig config;
	private Registry myRegistry;
	private ISendPackage impl;
	private int shortestString;
	private String[] itok;
	private boolean useSyns;
	private CommonLogger clogger;
	private File WNDir;
	private IRAMDictionary dict;
	private int myClientID;
	private boolean iGotTheFullList1;
	private boolean iGotTheFullList2;
	private IWorkPackage myWP;
	private ArrayList<String> fullList;
	private ArrayList<String> wpList;
	private int seq1;
	private int seq2;
	private int maxgap;
	private String[] tempsplit;
	private String fullLeftSource;
	private String fullLeftDest;
	private String fullRightSource;
	private String fullRightDest;
	private int currentLeftSequence;
	private int currentRightSequence;
	private String[] leftSourceTokens;
	private String[] leftDestTokens;
	private String[] rightSourceTokens;
	private String[] rightDestTokens;
	private String[] splitByColon;
	private String[] splitByDot;
	private String[] tempTokens;
	private ArrayList<String> tokenAList;
	private ArrayList<String> tokenAListCleaned;
	private ArrayList<String> colonDotAList;	
	private List<String> splitByColonList;
	private Set<String> removeDuplicates;
	private long tempcounter=0;
	private long outerloopcounter=0;
	private long innerloopcounter=0;
	private long totalcounter =0;
	private HashMap<String, ArrayList<String>> leftsource;
	private HashMap<String, ArrayList<String>> leftdest;
	private HashMap<String, ArrayList<String>> rightsource;
	private HashMap<String, ArrayList<String>> rightdest;
	private ArrayList<String[]> fullListTokens;
	private ArrayList<ArrayList<String[]>> fullListTokenArray;
	private ArrayList<String> wordtypes;
	private ArrayList<String> resultList;
	private IIndexWord idxWord;
	private IWordID wordID;
	private IWord word;
	private ISynset synset;
	private boolean fullListTokenized;
	private boolean isGapRun;
	
	public static void main(String[] args) {
		ClientMain client = new ClientMain();
		try {
			client.execute();
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	public ClientMain() {
		try {
			config = new PropertyConfig();
			isGapRun = config.activateGap();
			myRegistry = LocateRegistry.getRegistry(config.getServer(), config.getServerPort());
			impl = (ISendPackage) myRegistry.lookup("AnalyzeServer");
			shortestString=config.getMinChars();
			itok=config.getIgnoreTokens();
			useSyns=config.useSynonyms();
			clogger = new CommonLogger(config.getLogFileName()+this.getClass().getSimpleName()+ManagementFactory.getRuntimeMXBean().getName());
			myClientID=impl.getMyClientIDOnce();
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
		if(useSyns){
			WNDir = new File(config.getWordNetDB());
			dict = new RAMDictionary (WNDir , ILoadPolicy.NO_LOAD );
			
			clogger.systemOutLogger("Loading Wordnet into memory ... ");
			long t = System.currentTimeMillis ();
			try {
				dict.open();
				dict.load(true);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			long t2=System.currentTimeMillis()-t;		
			clogger.systemOutLogger(" done "+Long.toString(t2));
		}
		iGotTheFullList1=false;
		iGotTheFullList2=false;
		resultList = new ArrayList<String>();
		leftsource = new HashMap<String, ArrayList<String>>();
		leftdest = new HashMap<String, ArrayList<String>>();
		rightsource = new HashMap<String, ArrayList<String>>();
		rightdest = new HashMap<String, ArrayList<String>>();
		fullListTokens = new ArrayList<String[]>();
		fullListTokenArray = new ArrayList<ArrayList<String[]>>();
		fullListTokenized=false;
		tokenAList = new ArrayList<String>();
		tokenAListCleaned = new ArrayList<String>();
		colonDotAList = new ArrayList<String>();
		wordtypes = new ArrayList<String>(Arrays.asList("noun", "adverb", "verb", "adjective"));
	}
	
	private void execute() throws RemoteException{
		while(impl.isServerReady()) {
			int index =-1;
			int tempgap=0;
			String workingRightString = "";
			tempcounter=0;
			myWP=(IWorkPackage)impl.getWork(myClientID);
			if (myWP==null){
				myWP=(IWorkPackage)impl.getMissingPackage(myClientID);		
				if (myWP==null){
					clogger.systemOutLogger("I neither got a fresh package nor a resend package! Waiting some time");
					try {
						Thread.sleep(300000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					clogger.systemOutLogger("Got a resend operation with the ID "+Integer.toString(myWP.getPackageID()));
				}
			}
			if(myWP!=null) {
					//We have a package to work with!
					//First, get the Full List if we don not have the fitting one
					if (myWP.isForward() && !iGotTheFullList2){
						if (fullList !=null){
							fullList.clear();
						}
						fullList=impl.getFullList2();
						iGotTheFullList2=true;
						iGotTheFullList1=false;
						fullListTokenized=false;
						fullListTokenArray.clear();
						clogger.systemOutLogger("I got the fullList2");

					} else if (!myWP.isForward() && !iGotTheFullList1){
						if (fullList !=null){
							fullList.clear();
						}
						fullList=impl.getFullList1();
						iGotTheFullList2=false;
						iGotTheFullList1=true;
						fullListTokenized=false;
						fullListTokenArray.clear();
						clogger.systemOutLogger("I got the fullList2");
					}
					seq1=myWP.getStartsequence1();
					seq2=0; //doesn't really matter, full list is always 0
					maxgap=impl.getGap();
					clogger.systemOutLogger("I now have the ID "+Integer.toString(myWP.getPackageID())+" and I am working on seq1:"+Integer.toString(seq1)+", seq2:"+Integer.toString(seq2)+" forward is: "+Boolean.toString(myWP.isForward()));
					wpList=myWP.getLeft();
					int fullListSize=fullList.size();
					for (String s : wpList){
						index=0;
						tempsplit = s.split(">>");
						fullLeftSource = tempsplit[1];
						fullLeftDest = tempsplit[2];
						currentLeftSequence=Integer.parseInt(tempsplit[0]);
						tokenize(fullLeftSource, "leftSourceTokens");
						tokenize(fullLeftDest, "leftDestTokens");
						if (useSyns){
							synonize(new ArrayList<String>(Arrays.asList(leftSourceTokens)), "leftsource");
							synonize(new ArrayList<String>(Arrays.asList(leftDestTokens)), "leftdest");
						}
						if (!fullListTokenized){
							tokenizeFullListOnce();
						}
						int fullcounter=-1;
						while (fullcounter<fullListSize-1){
							fullcounter++;
							workingRightString=fullList.get(fullcounter);
							tempsplit=workingRightString.split(">>");
							fullRightSource = tempsplit[1];
							fullRightDest = tempsplit[2];
							currentRightSequence=fullcounter;
							rightSourceTokens=fullListTokenArray.get(fullcounter).get(0);
							rightDestTokens=fullListTokenArray.get(fullcounter).get(1);
							compare();
							if (useSyns){
								synonize(new ArrayList<String>(Arrays.asList(rightSourceTokens)), "rightsource");
								synonize(new ArrayList<String>(Arrays.asList(rightDestTokens)), "rightdest");
								compareWSyns();
							}
							index++;
							if (fullListSize<index+maxgap){
								tempgap=fullListSize-index;
							} else {
								tempgap=maxgap;
							}
							int gapc=0;
							if(isGapRun){ //only do this if we want a gap!!
								if (fullLeftSource.equals(fullRightSource)){
									for (int i=index;i<index+tempgap;i++){	
										workingRightString = fullList.get(i);
										tempsplit=workingRightString.split(">>");
										fullRightDest = tempsplit[2];
										currentRightSequence=i;
										rightSourceTokens=fullListTokenArray.get(fullcounter).get(0);
										rightDestTokens=fullListTokenArray.get(i).get(1);
										compare();
										if (useSyns){
											synonize(new ArrayList<String>(Arrays.asList(rightSourceTokens)), "rightsource");
											synonize(new ArrayList<String>(Arrays.asList(rightDestTokens)), "rightdest");
											compareWSyns();
										}
										
									}
							 }
							}
						}
					}
					clogger.systemOutLogger("I ran the following amount of compares: "+Long.toString(tempcounter)+"::"+Long.toString(outerloopcounter)+"::"+Long.toString(innerloopcounter)+"::"+Long.toString(totalcounter));
					sendResult();
					
				}
			}
		clogger.systemOutLogger("All Done, Bye Bye!");
	}
	
	private void tokenizeFullListOnce(){

		int countup=0;
		String[] fullSourceList;
		String[] fullDestList;
		for (String f : fullList){
			String[] fulltempsplit=f.split(">>");
			String RightSource = fulltempsplit[1];
			String RightDest = fulltempsplit[2];
			fullSourceList=tokenize(RightSource, "rightSourceTokens");
			fullDestList=tokenize(RightDest, "rightDestTokens");
			fullListTokens = new ArrayList<String[]>();
			fullListTokens.add(fullSourceList);
			fullListTokens.add(fullDestList);
			fullListTokenArray.add(fullListTokens);
			countup++;
		}
		clogger.systemOutLogger("I fully tokenized the fullList, its size is "+Integer.toString(countup));
		fullListTokenized=true;
	}
	private String[] tokenize(String fullString, String whichtoken) {
		String [] tokenarray;
		//Create tokens
		splitByColon=fullString.split(":");
		splitByColonList = new ArrayList<String>(Arrays.asList(splitByColon));
		splitByColonList.removeAll(Collections.singleton(null));
		splitByColonList.removeAll(Collections.singleton(":"));
		splitByColonList.removeAll(Collections.singleton(""));
		splitByColon = splitByColonList.toArray(new String[splitByColonList.size()]);
		for (String s : splitByColon) {
			splitByDot=s.split("\\.");
			for (String f : splitByDot){
				colonDotAList.add(f);
			}
		}
		for (String x : colonDotAList){
			tempTokens = x.split("(?=\\p{Lu})");
			for (String i : tempTokens) {
				tokenAList.add(i);
			}
		}
		tokenAList.removeAll(Collections.singleton("")); //remove all empty
		for (String z : tokenAList){ //remove all string shorter than 3 symbols
			if (z.length() >shortestString) {
				tokenAListCleaned.add(z);
			}
		}
		removeDuplicates = new HashSet<String>(tokenAListCleaned);
		tokenarray = removeDuplicates.toArray(new String[removeDuplicates.size()]);
		tokenAList.clear();
		tokenAListCleaned.clear();
		colonDotAList.clear();
		removeDuplicates.clear();
		if (whichtoken.equals("leftSourceTokens")){
			leftSourceTokens = tokenarray;
			return tokenarray;
		} else if (whichtoken.equals("leftDestTokens")){
			leftDestTokens = tokenarray;
			return tokenarray;
		} else if (whichtoken.equals("rightSourceTokens")) {
			rightSourceTokens = tokenarray;
			return tokenarray;
		} else if (whichtoken.equals("rightDestTokens")) {
			rightDestTokens = tokenarray;
			return tokenarray;
		}
		return null;
	}
	
	private void synonize(ArrayList<String> listtosyn, String which) {
		//find synonyms
		ArrayList<String> temp;
		ArrayList<String> tempeff;
		String onestring="";
		ISynset tempsyn;
		for (String master : listtosyn){
			temp = new ArrayList<String>();
			onestring=master;
			temp.add(onestring);
			tempeff=findEfficientSame(master);
			if (tempeff==null){
				for (String wtype : wordtypes){
					tempsyn=getSynsType(wtype, master);
					if (tempsyn!=null){
						for( IWord w : tempsyn.getWords ()) {
							onestring=w.getLemma();
							if (!temp.contains(onestring)){
								temp.add(onestring);
							}
						}
					}
				}
			} else {
				temp=tempeff;
			}
			if (which.equalsIgnoreCase("leftsource")){
				leftsource.put(master, temp);
			} else if (which.equalsIgnoreCase("leftdest")){
				leftdest.put(master, temp);
			} else if (which.equalsIgnoreCase("rightsource")){
				rightsource.put(master, temp);
			} else if (which.equalsIgnoreCase("rightdest")){
				rightdest.put(master, temp);
			}
		}
		
		
	}
	
	private HashSet<String> cleanUpSynonymEq(Set<String> intersect){
		ISynset tempsyn;
		boolean isSingle=true;
		HashSet<String> tempset = new HashSet<String>();
		for (String t: intersect){
			tempset.add(t);//copy
		}
		for (String x : intersect){
			String onestring;
			for (String wtype : wordtypes){
				tempsyn=getSynsType(wtype, x);
				if (tempsyn!=null){
					for (IWord w: tempsyn.getWords()){
						onestring=w.getLemma();
						if (tempset.contains(onestring)){
							tempset.remove(onestring);
						}
					}
				}
			}
		}
		return tempset;
	}
	private ISynset getSynsType(String type, String master){
		if (type.equalsIgnoreCase("noun")){
			idxWord = dict.getIndexWord (master, POS.NOUN );
		} else if (type.equalsIgnoreCase("adverb")){
			idxWord = dict.getIndexWord (master, POS.ADVERB );
		} else if (type.equalsIgnoreCase("adjective")){
			idxWord = dict.getIndexWord (master, POS.ADJECTIVE );
		} else if (type.equalsIgnoreCase("verb")){
			idxWord = dict.getIndexWord (master, POS.VERB );
		}
		if (idxWord!=null){
			wordID = idxWord.getWordIDs().get(0) ;
			word = dict.getWord(wordID);
			synset = word.getSynset();
			return synset;
		} else {
			return null;
		}
		
	}
	private void compare() {
		tempcounter++;
		outerloop:
		for (String s : leftSourceTokens){
			outerloopcounter++;
			totalcounter++;
			if (filterToken(s) && Arrays.asList(rightSourceTokens).contains(s)){
				//We found a matching token, let's see if we find one in the other side too
				for (String e : leftDestTokens){
					innerloopcounter++;
					totalcounter++;
					if (filterToken(e) && Arrays.asList(rightDestTokens).contains(e)){
						//We found something matching!
							resultList.add(Integer.toString(currentLeftSequence)+":"+Integer.toString(currentRightSequence)+":"+s+":"+e+":NONE:NONE");
					}
				}
			}
		}
	}
	
	private void compareWSyns(){
		String resultString="";
		ArrayList<String> temparrl;
		ArrayList<String> temparrl2;
		ArrayList<String> allrightvalssource = new ArrayList<String>();
		ArrayList<String> allrightvaldest = new ArrayList<String>();
		Set<String> intersect = new HashSet<String>();
		Set<String> intersectdest = new HashSet<String>();
		for (String s : leftSourceTokens){
			temparrl=leftsource.get(s);
			for (String ix : temparrl){
				if (filterToken(ix)){
					intersect.add(ix);
				}
			}
		}
		for (String s : rightSourceTokens){
			temparrl2=rightsource.get(s);
			for (String ix : temparrl2){
				if (filterToken(ix)){
					allrightvalssource.add(ix);
				}
			}
		}
		intersect.retainAll(allrightvalssource);
		//Todo: Filter synonym equivalence
		if (intersect.size()>1){
			intersect=cleanUpSynonymEq(intersect);
		}
		if (intersect.size()>0){
			for (String s : leftDestTokens){
				temparrl=leftdest.get(s);
				for (String ix : temparrl){
					if (filterToken(ix)){
						intersectdest.add(ix);
					}
				}
			}
			for (String s : rightDestTokens){
				temparrl2=rightdest.get(s);
				for (String ix : temparrl2){
					if (filterToken(ix)){
						allrightvaldest.add(ix);
					}
				}
			}
			intersectdest.retainAll(allrightvaldest);
			//Todo: Filter synonym equivalence
			intersectdest=cleanUpSynonymEq(intersectdest);
		}
		if (intersect.size()>0 && intersectdest.size()>0){
			for (String s: intersect){
				for (String e : intersectdest){
					resultString = Integer.toString(currentLeftSequence)+":"+Integer.toString(currentRightSequence)+":"+s+":"+e+":NONE:NONE";
					if (!resultList.contains(resultString)){
						resultList.add(resultString);
					}
				}
			}
		}
		
	}
	private boolean filterToken(String token){
		boolean isok =true;
		if (Arrays.asList(itok).contains(token)){ //filter for tokens to ignore
			isok=false;
		}
		return isok;
	}
	
	private ArrayList<String> findEfficientSame(String master){
		//If we already did a synonym lookup somewhere, we do not need to do it again, the result will always be the same!
		if (leftsource.containsKey(master)){
			return leftsource.get(master);
		} else if (leftdest.containsKey(master)){
			return leftdest.get(master);
		} else if (rightsource.containsKey(master)){
			return rightsource.get(master);
		} else if (rightdest.containsKey(master)){
			return rightdest.get(master);
		} else {
			return null;
		}
	}
	
	private void debugPrintSynonyms(){
		System.out.println("The synonyms for leftsource:");
		for (Entry<String, ArrayList<String>> e: leftsource.entrySet()){
			if (e.getValue().size()>1){
				System.out.print(e.getKey()+": ");
				for (String x : e.getValue()){
					System.out.print(x+", ");
				}
				System.out.println("");
			}
		}
		System.out.println("The synonyms for leftdest:");
		for (Entry<String, ArrayList<String>> e: leftdest.entrySet()){
			if (e.getValue().size()>1){
				System.out.print(e.getKey()+": ");
				for (String x : e.getValue()){
					System.out.print(x+", ");
				}
				System.out.println("");
			}
		}
		System.out.println("The synonyms for rightsource:");
		for (Entry<String, ArrayList<String>> e: rightsource.entrySet()){
			if (e.getValue().size()>1){
				System.out.print(e.getKey()+": ");
				for (String x : e.getValue()){
					System.out.print(x+", ");
				}
				System.out.println("");
			}
		}
		clogger.systemOutLogger("The synonyms for rightdest:");
		for (Entry<String, ArrayList<String>> e: rightdest.entrySet()){
			if (e.getValue().size()>1){
				System.out.print(e.getKey()+": ");
				for (String x : e.getValue()){
					System.out.print(x+", ");
				}
				clogger.systemOutLogger("");
			}
		}
	}
	private void sendResult() throws RemoteException{
		myWP.setResult(resultList);
		try {
			impl.returnResult((IWorkPackage)myWP);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		clogger.systemOutLogger("result Sent");
		if (resultList != null) {
        	resultList.clear();
        }
		
	}
}
