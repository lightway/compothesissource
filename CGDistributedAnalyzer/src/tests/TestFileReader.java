package tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import utilities.PropertyConfig;

public class TestFileReader {
	private File testFile1;
	private File testFile2;
	private static PropertyConfig config;
	private String fileN1;
	private String fileN2;
	
	public TestFileReader() {
		config = new PropertyConfig();
		fileN1=config.getTestFile1();
		fileN2=config.getTestFile2();
		testFile1 = new File(fileN1);
		testFile2 = new File(fileN2);
	}
	
	public ArrayList<String> getTestList1() {
		ArrayList<String> temp1 = new ArrayList<String>();
		try {
			Scanner scan = new Scanner(testFile1);
			while (scan.hasNext()){
			    temp1.add(scan.next());
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return temp1;
	}
	
	public ArrayList<String> getTestList2() {
		ArrayList<String> temp2 = new ArrayList<String>();
		try {
			Scanner scan = new Scanner(testFile2);
			while (scan.hasNext()){
			    temp2.add(scan.next());
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return temp2;
	}
}
