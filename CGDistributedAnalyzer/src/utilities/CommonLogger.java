package utilities;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class CommonLogger {
	PropertyConfig config;
	private PrintWriter logwriter;
	private long currentmillis;
	
	public CommonLogger(String logfilename){
		config = new PropertyConfig();
		try {
			logwriter = new PrintWriter(logfilename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	public void systemOutLogger(String mess){
		currentmillis = System.currentTimeMillis();
		System.out.println(Long.toString(currentmillis)+": "+mess);
		if (config.activateLogging()){
			logwriter.println(Long.toString(currentmillis)+": "+mess);
			logwriter.flush();
		}
	}
}
