package utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyConfig {

	private Properties props;
	String configfile;
	public PropertyConfig() {
		props = new Properties();
		configfile = "config.properties";
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configfile);
		try {
			props.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getServer() {
		return props.getProperty("server");
	}
	public int getServerPort() {
		return Integer.parseInt(props.getProperty("port"));
	}
	public String getOutputFileName(){
		return props.getProperty("outputfile");
	}
	public int getGap() {
		return Integer.parseInt(props.getProperty("gap"));
	}
	public int getMinChars(){
		return Integer.parseInt(props.getProperty("minchars"));
	}
	public String getDBName1(){
		return props.getProperty("dbname1");
	}
	public String getDBName2(){
		return props.getProperty("dbname2");
	}
	public String[] getIgnoreTokens() {
		String[] itok = props.getProperty("ignoretokens").split(",");
		return itok;
	}
	public String[] getPackageFilter() {
		String[] pkgfilter = props.getProperty("packagefilter").split(";");
		return pkgfilter;
	}
	public boolean getIsTestRun() {
		if (props.getProperty("istestrun").equalsIgnoreCase("true")){
			return true;
		} else {
			return false;
		}
	}
	public String getTestFile1() {
		return props.getProperty("testfile1");
	}
	public String getTestFile2() {
		return props.getProperty("testfile2");
	}
	
	public String getResultFileFull() {
		return props.getProperty("resultsfull");
	}
	
	public String getResultFileToken() {
		return props.getProperty("resultstoken");
	}
	
	public boolean doResultsCalc() {
		if (props.getProperty("doresults").equalsIgnoreCase("true")){
			return true;
		} else {
			return false;
		}
	}
	
	public boolean useSynonyms() {
		if (props.getProperty("enablesyns").equalsIgnoreCase("true")){
			return true;
		} else {
			return false;
		}
	}
	public String getWordNetDB(){
		return props.getProperty("wordnetfolder");
	}
	
	public boolean doManualAbort() {
		if (props.getProperty("manualabort").equalsIgnoreCase("true")){
			return true;
		} else {
			return false;
		}
	}
	
	public boolean activateLogging() {
		if (props.getProperty("enablelog").equalsIgnoreCase("true")){
			return true;
		} else {
			return false;
		}
	}
	
	public boolean activateGap() {
		if (props.getProperty("isGapRun").equalsIgnoreCase("true")){
			return true;
		} else {
			return false;
		}
	}
	public String getLogFileName(){
		return props.getProperty("logfile");
	}
	public int getExpNodes(){
		return Integer.parseInt(props.getProperty("expectednodes"));
	}
	public Long getPackageTimeout(){
		return Long.parseLong(props.getProperty("packagetimeout"));
	}
	
	public int getMaxSegmentation(){
		return Integer.parseInt(props.getProperty("maxsegmentationsize"));
	}
}
