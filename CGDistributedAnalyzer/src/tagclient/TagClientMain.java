package tagclient;

import interfaces.ISendPackage;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.math.BigInteger;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import server.CalcSums;
import utilities.CommonLogger;
import utilities.PropertyConfig;

public class TagClientMain {
	private PropertyConfig config;
	private Registry myRegistry;
	private ISendPackage impl;
	private CommonLogger clogger;
	private int myClientID;
	private ArrayList<String> fullList1;
 	private HashMap<String, Integer> allStringsMap;
 	private int shortestString;
 	private PrintWriter writeit;
 	private long counter;
	public static void main(String[] args) {
		TagClientMain tagger = new TagClientMain();
		try {
			tagger.calculateList1();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public TagClientMain(){
		config = new PropertyConfig();
		shortestString=config.getMinChars();
		try {
			myRegistry = LocateRegistry.getRegistry(config.getServer(), config.getServerPort());
			impl = (ISendPackage) myRegistry.lookup("AnalyzeServer");
			myClientID=impl.getMyClientIDOnce();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		clogger = new CommonLogger(config.getLogFileName()+this.getClass().getSimpleName()+ManagementFactory.getRuntimeMXBean().getName());
		allStringsMap = new HashMap<String, Integer>();
		counter=0;
	}
	
	private void calculateList1() throws RemoteException{
		fullList1 = impl.getFullList1();
		clogger.systemOutLogger("Got the full List!");
		tokenizeList1();
		clogger.systemOutLogger("Finished tokenizing full list!");
		writeCountedList();
		clogger.systemOutLogger("Wrote everything to output file!");
		
	}

	private void tokenizeList1() {
		String [] tokenarray;
		//Create tokens
		ArrayList<String> tempList1;
		String[] splitBySide;
		String[] splitByColonLeft;
		String[] splitByColonRight;
		ArrayList<String> splitByColonLeftList;
		ArrayList<String> splitByColonRightList;
		String[] splitByDotLeft;
		String[] splitByDotRight;
		ArrayList<String> colonDotAListLeft;
		ArrayList<String> colonDotAListRight;
		String[] tempTokensLeft;
		String[] tempTokensRight;
		ArrayList<String> tokenAListLeft;
		ArrayList<String> tokenAListRight;
		ArrayList<String> tokenAListCleanedLeft;
		ArrayList<String> tokenAListCleanedRight;
		for (String fullString : fullList1){
			tokenAListLeft = new ArrayList<String>();
			tokenAListRight = new ArrayList<String>();
			colonDotAListLeft = new ArrayList<String>();
			colonDotAListRight = new ArrayList<String>();
			tokenAListCleanedLeft = new ArrayList<String>();
			tokenAListCleanedRight = new ArrayList<String>();
			tempList1=new ArrayList<String>();
			splitBySide = fullString.split(">>");
			splitByColonLeft=splitBySide[1].split(":");
			splitByColonRight = splitBySide[2].split(":");
			splitByColonLeftList = new ArrayList<String>(Arrays.asList(splitByColonLeft));
			splitByColonRightList= new ArrayList<String>(Arrays.asList(splitByColonRight));
			splitByColonLeftList.removeAll(Collections.singleton(null));
			splitByColonLeftList.removeAll(Collections.singleton(":"));
			splitByColonLeftList.removeAll(Collections.singleton(""));
			splitByColonRightList.removeAll(Collections.singleton(null));
			splitByColonRightList.removeAll(Collections.singleton(":"));
			splitByColonRightList.removeAll(Collections.singleton(""));
			splitByColonLeft = splitByColonLeftList.toArray(new String[splitByColonLeftList.size()]);
			splitByColonRight = splitByColonRightList.toArray(new String[splitByColonRightList.size()]);
			for (String s : splitByColonLeft) {
				splitByDotLeft=s.split("\\.");
				for (String f : splitByDotLeft){
					colonDotAListLeft.add(f);
				}
			}
			for (String s : splitByColonRight) {
				splitByDotRight=s.split("\\.");
				for (String f : splitByDotRight){
					colonDotAListRight.add(f);
				}
			}
			for (String x : colonDotAListLeft){
				tempTokensLeft = x.split("(?=\\p{Lu})");
				for (String i : tempTokensLeft) {
					tokenAListLeft.add(i);
				}
			}
			for (String x : colonDotAListRight){
				tempTokensRight = x.split("(?=\\p{Lu})");
				for (String i : tempTokensRight) {
					tokenAListRight.add(i);
				}
			}
			tokenAListLeft.removeAll(Collections.singleton("")); //remove all empty
			tokenAListRight.removeAll(Collections.singleton("")); //remove all empty
			for (String z : tokenAListLeft) {
				if (z.length() >shortestString){
					tokenAListCleanedLeft.add(z);
				}
			}
			for (String z : tokenAListRight) {
				if (z.length() >shortestString){
					tokenAListCleanedRight.add(z);
				}
			}
			for (String a : tokenAListCleanedLeft){
				addOrAppend(a);
				counter++;
			}
			for (String a : tokenAListCleanedRight){
				addOrAppend(a);
				counter++;
			}
		}
	}
	
	private void addOrAppend(String s){
		if (allStringsMap.containsKey(s)){
			int oldvalue = allStringsMap.get(s);
			oldvalue++;
			allStringsMap.put(s, oldvalue);
		} else {
			allStringsMap.put(s, 1);
		}
	}
	
	private void writeCountedList(){
		try {
			writeit = new PrintWriter(config.getOutputFileName()+"_tagger");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		for (Entry<String, Integer> e: allStringsMap.entrySet()){
			writeit.println(e.getKey()+":"+Integer.toString(e.getValue()));
		}
		writeit.println("Total count of tokens: "+Long.toString(counter));
		writeit.flush();
		writeit.close();
	}

}
