package cdaverification;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DatabaseChecker {
	private Connection connect = null;
	private String getObjectID = "select id from callobject where packageName=? and className =?";
	private String getAllCallOccurrences = "select * from calls where caller =? and callee=?";
	private String getAllCalls ="select caller, callee, callermethod, calleemethod from calls";
	private String getNameOfCallParts = "select packageName, className from callobject where id =?";
	private String getMethodNameByID = "select methodname from comethod where methodid=?";
	private PreparedStatement getObjectIDPrep;
	private PreparedStatement getAllCallOccurrencesPrep;
	private PreparedStatement getAllCallsPrep;
	private PreparedStatement getNameOfCallPartsPrep;
	private PreparedStatement getMethodNameByIDPrep;
	private ResultSet objIDRS;
	private ResultSet allFindings;
	private ResultSet allCalls;
	private ResultSet nameOfParts;
	private ResultSet nameOfMethods;
	private ArrayList<String> fullList;
	private String objName;
	private String mName;
	
	public DatabaseChecker(String dbName) {
		fullList = new ArrayList<String>(4000000);
		try {
			connect = DriverManager
			          .getConnection("jdbc:mysql://127.0.0.1:3306/"+dbName+"?"
			              + "user=root&password=abc123");
			getObjectIDPrep = connect.prepareStatement(getObjectID);
			getAllCallOccurrencesPrep = connect.prepareStatement(getAllCallOccurrences);
			getAllCallsPrep = connect.prepareStatement(getAllCalls);
			getNameOfCallPartsPrep = connect.prepareStatement(getNameOfCallParts);
			getMethodNameByIDPrep = connect.prepareStatement(getMethodNameByID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int checkOccurrence(String source, String dest) { //returns count of matching occurrences of caller and callee
		int occounter =0;
		int sourceobjeid=0;
		int destobjid=0;
		String[] sourcearr = source.split("\\.");
		String[] destarr = dest.split("\\.");
		String sourcePackageName = sourcearr[0];
		for (int i=1; i<sourcearr.length-1; i++) {
			sourcePackageName=sourcePackageName+"."+sourcearr[i];
		}
		String sourceClassName = sourcearr[sourcearr.length-1];
		sourceClassName=sourceClassName.replace('$', '_');
		String destClassName = destarr[destarr.length-1];
		destClassName=destClassName.replace('$', '_');
		String destPackageName = destarr[0];
		for (int j=1; j<destarr.length-1; j++) {
			destPackageName=destPackageName+"."+destarr[j];
		}
		sourceobjeid=getObjectID(sourcePackageName, sourceClassName);
		destobjid=getObjectID(destPackageName, destClassName);
		if (sourceobjeid>0 && destobjid>0){
				try {
				if (getAllCallOccurrencesPrep != null) {
					getAllCallOccurrencesPrep.clearParameters();
				}
				
				getAllCallOccurrencesPrep.setInt(1, sourceobjeid);
				getAllCallOccurrencesPrep.setInt(2, destobjid);
				allFindings = getAllCallOccurrencesPrep.executeQuery();
				while (allFindings.next()) {
					occounter++;
				}
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
			
		
		return occounter;
		
	}
	
	private int getObjectID(String packageName, String className){
		int oid=0;
		try {
			if (getObjectIDPrep != null) {
				getObjectIDPrep.clearParameters();
			}
			getObjectIDPrep.setString(1, packageName);
			getObjectIDPrep.setString(2, className);
			objIDRS = getObjectIDPrep.executeQuery();
			int resultcounter =0;
			while(objIDRS.next()) {
				oid=objIDRS.getInt(1);
				resultcounter++;
			}
			if (resultcounter >1 || resultcounter == 0){
				//System.out.println("Warning, possible Database inconsistence in table callobjects detected, the result of the counter is "+Integer.toString(resultcounter)+", I wanted to find in there: "+packageName+":"+className);
			} else {
				//System.out.println("I found "+packageName+":"+className+"!!!");
			}
			
		} catch (SQLException w) {
			w.printStackTrace();
		}
		return oid;
	}
	
	private String getObjectNameByID(int id) {
		try {
			if (getNameOfCallPartsPrep != null) {
				getNameOfCallPartsPrep.clearParameters();
			}
			getNameOfCallPartsPrep.setString(1, Integer.toString(id));
			nameOfParts = getNameOfCallPartsPrep.executeQuery();
			int counter =0;
			while (nameOfParts.next()){
				counter++;
				objName=nameOfParts.getString(1)+":"+nameOfParts.getString(2);
			}
			if (counter == 0) {
				System.out.println("Warning! No Matching Callobject for ID "+Integer.toString(id)+" found!");
			} else if (counter >1){
				System.out.println("Warning! Possible DB inconsistence for ID "+Integer.toString(id)+" detected!");
			}
		} catch (SQLException g) {
			g.printStackTrace();
		}
		return objName;
	}
	
	private String getMethodNameByID(int mId){
		try {
			if (getMethodNameByIDPrep != null){
				getMethodNameByIDPrep.clearParameters();
			}
			getMethodNameByIDPrep.setString(1, Integer.toString(mId));
			nameOfMethods = getMethodNameByIDPrep.executeQuery();
			int mcounter=0;
			while (nameOfMethods.next()){
				mcounter++;
				mName = nameOfMethods.getString(1);
			}
			if (mcounter ==0){
				System.out.println("Warning! No Matching MethodID for methodid "+Integer.toString(mId)+" found!");				
			} else if (mcounter >1){
				System.out.println("Warning! Possible DB inconsistence for methodid "+Integer.toString(mId)+" found!");
			}
		} catch (SQLException f){
			f.printStackTrace();
		}
		return mName;
	}
	public ArrayList<String> getFullList() {
		int callerID =0;
		int calleeID = 0;
		String caller = "";
		String callee = "";
		try {
			if (getAllCallsPrep != null) {
				getAllCallsPrep.clearParameters();
			}
			allCalls = getAllCallsPrep.executeQuery();
			while(allCalls.next()){
				callerID = allCalls.getInt(1);
				calleeID= allCalls.getInt(2);
				caller = getObjectNameByID(callerID);
				callee = getObjectNameByID(calleeID);
				fullList.add(caller+">>"+callee);
			}
		} catch (SQLException v) {
			v.printStackTrace();
		}
		return fullList;
	}
	
	public ArrayList<String> getFullListWithMethods(){
		int callerID =0;
		int calleeID = 0;
		int callerMethodID=0;
		int calleeMethodID=0;
		String caller = "";
		String callee = "";
		String callerMethod ="";
		String calleeMethod="";
		try {
			if (getAllCallsPrep != null) {
				getAllCallsPrep.clearParameters();
			}
			allCalls = getAllCallsPrep.executeQuery();
			while(allCalls.next()){
				callerID = allCalls.getInt(1);
				calleeID= allCalls.getInt(2);
				callerMethodID=allCalls.getInt(3);
				calleeMethodID=allCalls.getInt(4);
				caller = getObjectNameByID(callerID);
				callee = getObjectNameByID(calleeID);
				callerMethod=getMethodNameByID(callerMethodID);
				calleeMethod=getMethodNameByID(calleeMethodID);
				fullList.add(caller+"::"+callerMethod+">>"+callee+"::"+calleeMethod);
			}
		} catch (SQLException v) {
			v.printStackTrace();
		}
		if (connect!=null){
			try {
				connect.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return fullList;
	}
}
