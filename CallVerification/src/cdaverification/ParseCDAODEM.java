package cdaverification;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ParseCDAODEM {
	private File odem;
	private HashMap<String, Stack<String>> results;
	public ParseCDAODEM(String filename) {
		odem = new File(filename);
		results = new HashMap<String, Stack<String>>();
		
	}
	
	public HashMap<String, Stack<String>> parseODEM() {
		DocumentBuilderFactory dbfact = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dbuilder = dbfact.newDocumentBuilder();
			Document odemdoc = dbuilder.parse(odem);
			odemdoc.getDocumentElement().normalize();
			NodeList rootnodes = odemdoc.getElementsByTagName("type");
			int counter =0;
			for (int i=0; i<rootnodes.getLength(); i++){
				Node snode = rootnodes.item(i);
				if (snode.getNodeType() == Node.ELEMENT_NODE) {
					Element selement = (Element) snode;
					System.out.println(selement.getAttribute("name"));
					Stack<String> tempStack = new Stack<String>();
					counter++;
					for (String s : getDependencies(selement)) {
						System.out.println("  =>"+s);
						tempStack.add(s);
					}
					results.put(selement.getAttribute("name"), tempStack);
					
				}
			}
			System.out.println("There are "+Integer.toString(counter)+" Values in the ODEM!");
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return results;
	}
	
	private ArrayList<String> getDependencies(Element element) {
		ArrayList<String> temp = new ArrayList<String>();
		NodeList deps = element.getElementsByTagName("depends-on");
		for (int j=0; j<deps.getLength(); j++) {
			Node onedep = deps.item(j);
			if (onedep.getNodeType() == Node.ELEMENT_NODE){
				Element onedepel = (Element) onedep;
				temp.add(onedepel.getAttribute("name"));
				
			}
			
		}
		return temp;
	}

}
