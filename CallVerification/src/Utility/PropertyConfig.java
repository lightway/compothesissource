package Utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyConfig {

	private Properties props;
	String configfile;
	public PropertyConfig() {
		props = new Properties();
		configfile = "config.properties";
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configfile);
		try {
			props.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getServer() {
		return props.getProperty("server");
	}
	public int getServerPort() {
		return Integer.parseInt(props.getProperty("port"));
	}
	public String getOutputFileName(){
		return props.getProperty("outputfile");
	}
	public int getGap() {
		return Integer.parseInt(props.getProperty("gap"));
	}
	public boolean activateLogging() {
		if (props.getProperty("enablelog").equalsIgnoreCase("true")){
			return true;
		} else {
			return false;
		}
	}
	public String getDBName1(){
		return props.getProperty("dbname1");
	}
	public String getDBName2(){
		return props.getProperty("dbname2");
	}
}
