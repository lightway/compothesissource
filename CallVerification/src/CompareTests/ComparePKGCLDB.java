package CompareTests;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.text.StyledEditorKit.BoldAction;

import Utility.CommonLogger;
import Utility.PropertyConfig;
import cdaverification.DatabaseChecker;

public class ComparePKGCLDB implements Runnable{
	private Thread t;
	private int gap;
	private float percentagex;
	private boolean reverse;
	private CommonLogger output;
	private PropertyConfig config;
	private static ArrayList<String> fullList1;
	private static ArrayList<String> fullList2;
	
	public ComparePKGCLDB(int gap, boolean reverse){
		this.gap=gap;
		this.reverse=reverse;
		config=new PropertyConfig();
		output=new CommonLogger(config.getOutputFileName()+Boolean.toString(reverse)+Integer.toString(gap));
		
	}
	public void run(){
		this.compare(gap);
		output.systemOutLogger("We have a hit rate of "+Float.toString(percentagex)+"%");
	}
	
	
	public static void fillLists(String db1, String db2) {
		DatabaseChecker dbchecker = new DatabaseChecker(db1);
		DatabaseChecker dbchecker2 = new DatabaseChecker(db2);
		fullList1 = dbchecker.getFullList();
		fullList2 = dbchecker2.getFullList();
	}
	public void compare(int gap){
		String callerPkg="";
		String callerCL="";
		int totalCounter=0;
		int matchCounter=0;
		int failedCounter=0;
		int counter1=0;
		int counter2=0;
		if (gap ==0){
			output.systemOutLogger("No gap selected, only comparing the larger against the smaller!");
			if (fullList1.size() >= fullList2.size()) {
				output.systemOutLogger("List 1 is bigger!");
				for (String s : fullList1){
					counter1++;
					if (fullList2.contains(s)){
						output.systemOutLogger("We have a direct match:"+s+":"+Integer.toString(counter1));
						totalCounter++;
						matchCounter++;
					} else {
						totalCounter++;
						failedCounter++;
					}
				}
			} else {
				System.out.println("List 2 is bigger!");
				for (String x : fullList2) {
					counter1++;
					if (fullList1.contains(x)) {
						output.systemOutLogger("We have a direct match:"+x+":"+Integer.toString(counter1));
						totalCounter++;
						matchCounter++;
					} else {
						totalCounter++;
						failedCounter++;
					}
				}
			}
		} else {
			output.systemOutLogger("We analyze with a max gap of "+Integer.toString(gap));
			//We add a gap to the search
			String[] split;
			String[] split2;
			String[] splittemp;
			String fullSource="";
			String fullDest="";
			String fullSource2="";
			String fullDest2="";
			int index =0;
			counter1=0;
			int tempgap=gap;
			if (!reverse) {
				System.out.println("Not reversing!");				
				for (String s : fullList1){
					index=0;
					counter1++;
					counter2=0;
					split = s.split(">>");
					fullSource=split[0];
					for (String v : fullList2){
						counter2++;
						split2 = v.split(">>");
						fullSource2=split2[0];
						index++;
						if (fullSource.equalsIgnoreCase(fullSource2)) {
							fullDest=split[1];
							if (fullList2.size()<index+gap){
								tempgap=fullList2.size()-index;
							}
								for (int i=index;i<index+tempgap;i++){
									splittemp = fullList2.get(i).split(">>");
									fullDest2 = splittemp[1];
									if (fullDest.equalsIgnoreCase(fullDest2)){
										matchCounter++;
										totalCounter++;
										output.systemOutLogger("We have a match:"+s+":"+Integer.toString(counter1)+":"+Integer.toString(index)+":"+Integer.toString(i+1));
									}
								}
							
						} else {
							failedCounter++;
							totalCounter++;
						}
					}
					
				}
			} else {
				counter1=0;
				System.out.println("reversing");				
				for (String s : fullList2){
					index=0;
					counter1++;
					counter2=0;
					split = s.split(">>");
					fullSource=split[0];
					for (String v : fullList1){
						split2 = v.split(">>");
						fullSource2=split2[0];
						index++;
						if (fullSource.equalsIgnoreCase(fullSource2)) {
							fullDest=split[1];
							if (fullList1.size()<index+gap){
								tempgap=fullList1.size()-index;
							}
								for (int i=index;i<index+tempgap;i++){
									splittemp = fullList1.get(i).split(">>");
									fullDest2 = splittemp[1];
									if (fullDest.equalsIgnoreCase(fullDest2)){
										matchCounter++;
										totalCounter++;
										output.systemOutLogger("We have a reverse match:"+s+":"+Integer.toString(counter1)+":"+Integer.toString(index)+":"+Integer.toString(i+1));
									}
								}
							
						} else {
							failedCounter++;
							totalCounter++;
						}
					}
					
				}
			}
		}
		float percentage = ((float)matchCounter)/(float)totalCounter*100;
		output.systemOutLogger("We tested "+Integer.toString(totalCounter)+" entries!");
		this.percentagex= percentage;
	}
	public void start() {
		if (t == null) {
			t = new Thread(this, "bla");
			t.start();
		}
	}
}
