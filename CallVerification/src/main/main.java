package main;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import Utility.PropertyConfig;
import CompareTests.ComparePKGCLDB;
import cdaverification.*;

public class main {
	private static PropertyConfig config;
	public static void main(String[] args) {
		config = new PropertyConfig();
		ComparePKGCLDB.fillLists(config.getDBName1(), config.getDBName2());
		ComparePKGCLDB comp0 = new ComparePKGCLDB(0, false); //Direct without gap
		comp0.start();
		ComparePKGCLDB comp1 = new ComparePKGCLDB(config.getGap(), false);
		comp1.start();
		if (config.getGap()>0){
			ComparePKGCLDB comp2 = new ComparePKGCLDB(config.getGap(), true);
			comp2.start();
		}

	}

}
