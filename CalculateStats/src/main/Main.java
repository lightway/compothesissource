package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jxl.Workbook;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import statistics.CombinedDependency;
import statistics.SingleHappeningFilter;

public class Main {
	private static ArrayList<String> allPath = new ArrayList<String>();
	private static String fixPathPart = "C:\\Users\\User\\PATH\\TO\\Results\\";
	private static String cleaned = "Cleaned\\";
	private static String nogap = "NoGap-NoSyns\\";
	private static String withgap10 ="WithGap\\Gap10\\";
	private static String withgap3 = "WithGap\\Gap3\\";
	private static String syns = "NoGap-WithSyns\\";
	private static String quer = "Quer\\";
	private static ArrayList<String> allnogap = new ArrayList<String>();
	private static ArrayList<String> allcleanedgap1 = new ArrayList<String>();
	private static ArrayList<String> allcleanedgap10 = new ArrayList<String>();
	private static ArrayList<String> allwithgap10 = new ArrayList<String>();
	private static ArrayList<String> allsyns = new ArrayList<String>();
	private static String full ="fullresults.txt";
	private String tokens = "tokenresults.txt";
	private String outputfile="calculated.txt";
	private String outputfilel="calculated_left.txt";
	private String outputfiler="calculated_right.txt";
	private BufferedReader fullReader;
	private BufferedReader tokenReader;
	private static BufferedReader shfReader;
	private PrintWriter output;
	private PrintWriter outputr;
	private PrintWriter outputl;
	private WritableWorkbook workbook;
	public static void main(String[] args) {
		fillPath();
		//SingleHappeningFilter shf = new SingleHappeningFilter();
		for (String s : allPath){
			Main main = new Main(s);
			CombinedDependency cd = new CombinedDependency(main.getFullReader(), main.getTokenReader(), main.getOutput(), main.getOutputl(), main.getOutputr(), main.getWorkbook());
			cd.calculate();			
			try {
				main.getWorkbook().write();
				main.getWorkbook().close();;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (WriteException e) {
				e.printStackTrace();
			}

		}
		//Get single occurences for nogap
		SingleHappeningFilter shf = new SingleHappeningFilter();
		for (String x : allnogap){
			Main shfMain = new Main(x);
			try {
				shf.addCase(shfMain.getFullReader(), x);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		shf.calculateResults();
		
		
	}
	
	public Main(String workingCaseBase){
		try {
			fullReader = new BufferedReader(new FileReader(workingCaseBase+full));
			tokenReader = new BufferedReader(new FileReader(workingCaseBase+tokens));
			output=new PrintWriter(workingCaseBase+outputfile);
			outputl=new PrintWriter(workingCaseBase+outputfilel);
			outputr=new PrintWriter(workingCaseBase+outputfiler);
			workbook = Workbook.createWorkbook(new File(workingCaseBase+outputfile+".xls")); 
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private static void fillPath(){
		//NoGap-NoSyns
		allPath.add(fixPathPart+nogap+"ngdateierstellen1_gap1\\");
		allPath.add(fixPathPart+nogap+"nghelp1_gap1\\");
		allPath.add(fixPathPart+nogap+"ngloeschen1_gap1\\");
		allPath.add(fixPathPart+nogap+"ngsuchen1_gap1\\");
		allPath.add(fixPathPart+nogap+"ngumbennen1_gap1\\");
		
		allnogap.add(fixPathPart+nogap+"ngdateierstellen1_gap1\\");
		allnogap.add(fixPathPart+nogap+"nghelp1_gap1\\");
		allnogap.add(fixPathPart+nogap+"ngloeschen1_gap1\\");
		allnogap.add(fixPathPart+nogap+"ngsuchen1_gap1\\");
		allnogap.add(fixPathPart+nogap+"ngumbennen1_gap1\\");
		
		//NoGap-WithSyns
		allPath.add(fixPathPart+syns+"nghelp1_gap1_wsyns\\");
		allPath.add(fixPathPart+syns+"ngloeschen1_gap1_wsyns\\");
		allPath.add(fixPathPart+syns+"ngsuchen1_gap1_wsyns\\");
		allPath.add(fixPathPart+syns+"ngumbennen2_gap1_wsyns\\");
		
		allsyns.add(fixPathPart+syns+"nghelp1_gap1_wsyns\\");
		allsyns.add(fixPathPart+syns+"ngloeschen1_gap1_wsyns\\");
		allsyns.add(fixPathPart+syns+"ngsuchen1_gap1_wsyns\\");
		allsyns.add(fixPathPart+syns+"ngumbennen2_gap1_wsyns\\");
		
		//WithGap-10
		allPath.add(fixPathPart+withgap10+"nghelp2\\");
		allPath.add(fixPathPart+withgap10+"ngloeschen1\\");
		allPath.add(fixPathPart+withgap10+"ngsearch2\\");
		allPath.add(fixPathPart+withgap10+"ngumbennen1\\");
		
		allwithgap10.add(fixPathPart+withgap10+"nghelp2\\");
		allwithgap10.add(fixPathPart+withgap10+"ngloeschen1\\");
		allwithgap10.add(fixPathPart+withgap10+"ngsearch2\\");
		allwithgap10.add(fixPathPart+withgap10+"ngumbennen1\\");
		
		//WithGap-3
		allPath.add(fixPathPart+withgap3+"ngsearch2_gap3\\");
		
		//Cleaned-NoGap
		allPath.add(fixPathPart+cleaned+"ngdateierstellen_cleaned_gap1\\");
		allPath.add(fixPathPart+cleaned+"nghilfe_cleaned_gap1\\");
		allPath.add(fixPathPart+cleaned+"nghilfe_cleaned_gap1_wsyns\\");
		allPath.add(fixPathPart+cleaned+"ngloeschen_cleaned_gap1\\");
		allPath.add(fixPathPart+cleaned+"ngsuchen_cleaned_gap1\\");
		allPath.add(fixPathPart+cleaned+"ngumbennen_cleaned_gap1\\");
		
		allcleanedgap1.add(fixPathPart+cleaned+"ngdateierstellen_cleaned_gap1\\");
		allcleanedgap1.add(fixPathPart+cleaned+"nghilfe_cleaned_gap1\\");
		allcleanedgap1.add(fixPathPart+cleaned+"nghilfe_cleaned_gap1_wsyns\\");
		allcleanedgap1.add(fixPathPart+cleaned+"ngloeschen_cleaned_gap1\\");
		allcleanedgap1.add(fixPathPart+cleaned+"ngsuchen_cleaned_gap1\\");
		allcleanedgap1.add(fixPathPart+cleaned+"ngumbennen_cleaned_gap1\\");
		
		//Cleaned-Gap10
		allPath.add(fixPathPart+cleaned+"nghilfe_cleaned_gap10\\");
		allPath.add(fixPathPart+cleaned+"ngloeschen_cleaned_gap10\\");
		allPath.add(fixPathPart+cleaned+"ngsuchen_cleaned_gap10\\");
		allPath.add(fixPathPart+cleaned+"ngumbennen_cleaned_gap10\\");
		
		allcleanedgap10.add(fixPathPart+cleaned+"nghilfe_cleaned_gap10\\");
		allcleanedgap10.add(fixPathPart+cleaned+"ngloeschen_cleaned_gap10\\");
		allcleanedgap10.add(fixPathPart+cleaned+"ngsuchen_cleaned_gap10\\");
		allcleanedgap10.add(fixPathPart+cleaned+"ngumbennen_cleaned_gap10\\");
		
		
		//Quer-NoGap
		allPath.add(fixPathPart+quer+"quer_argouml_dateierstellen-argouml_suchen_cleaned_gap1\\");
		allPath.add(fixPathPart+quer+"quer_argouml_hilfe-argouml_umbennen_cleaned_gap1\\");
		allPath.add(fixPathPart+quer+"quer_argouml_loeschen-argouml_umbennen_cleaned_gap1\\");
		allPath.add(fixPathPart+quer+"quer_pycharm_dateierstellen-argouml_loeschen_cleaned_gap1\\");
		allPath.add(fixPathPart+quer+"quer_pycharm_hilfe-argouml_umbennen_cleaned_gap1\\");

		//Quer-Gap10
		allPath.add(fixPathPart+quer+"quer_argouml_hilfe-argouml_umbennen_cleaned_gap10\\");
		allPath.add(fixPathPart+quer+"quer_argouml_loeschen-argouml_umbennen_cleaned_gap10\\");
		allPath.add(fixPathPart+quer+"quer_pycharm_dateierstellen-argouml_loeschen_cleaned_gap10\\");
		allPath.add(fixPathPart+quer+"quer_pycharm_hilfe-argouml_umbennen_cleaned_gap10\\");

	}

	public BufferedReader getFullReader() {
		return fullReader;
	}

	public BufferedReader getTokenReader() {
		return tokenReader;
	}

	public PrintWriter getOutput() {
		return output;
	}

	public PrintWriter getOutputl() {
		return outputl;
	}

	public PrintWriter getOutputr() {
		return outputr;
	}

	public WritableWorkbook getWorkbook() {
		return workbook;
	}

}
