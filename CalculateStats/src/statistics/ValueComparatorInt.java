//from http://stackoverflow.com/questions/109383/how-to-sort-a-mapkey-value-on-the-values-in-java
package statistics;

import java.util.Comparator;
import java.util.Map;

class ValueComparatorInt implements Comparator<String> {

    Map<String, Integer> base;
    public ValueComparatorInt(Map<String, Integer> base) {
        this.base = base;
    }
  
    public int compare(String a, String b) {
        if (base.get(a) >= base.get(b)) {
            return -1;
        } else {
            return 1;
        }
    }
}