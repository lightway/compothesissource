package statistics;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;

import jxl.*;
import jxl.write.*;
import jxl.write.Number;
import jxl.write.biff.RowsExceededException;

public class CombinedDependency {
	private BufferedReader fullReader;
	private BufferedReader tokenReader;
	private PrintWriter output;
	private PrintWriter outputl;
	private PrintWriter outputr;
	private HashMap<String, Integer> full;
	private HashMap<String, Integer> token;
	private HashMap<String, Float> left;
	private HashMap<String, Float> right;
	private HashMap<String, Float> combined;
	private HashMap<String, Float> absolutmap;
	private WritableWorkbook workbook;
	WritableSheet sheetleft;
	WritableSheet sheetright;
	WritableSheet sheetcombined;
	WritableSheet absolut;
	private long totalSum;
	
	
	public CombinedDependency(BufferedReader fR, BufferedReader tR, PrintWriter ou, PrintWriter oul, PrintWriter our, WritableWorkbook wb){
		fullReader = fR;
		tokenReader=tR;
		output=ou;
		outputl=oul;
		outputr=our;
		workbook=wb;
		totalSum=0;
		absolut = workbook.createSheet("Absolut", 0);
		sheetcombined = workbook.createSheet("Combined", 1); 
		sheetleft = workbook.createSheet("Left", 2);
		sheetright = workbook.createSheet("Right", 3);
		full = new HashMap<String, Integer>();
		token = new HashMap<String, Integer>();
		left = new HashMap<String, Float>();
		right = new HashMap<String, Float>();
		combined = new HashMap<String, Float>();
		absolutmap = new HashMap<String, Float>();
		try {
			fillToken();
			fillFull();
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void calculate(){
		String rname;
		String lname;
		int value;
		int totalvaluel;
		int totalvaluer;
		float per1;
		float per2;
		float abs1;
		String[] splited;
		for (Entry<String,Integer> e: full.entrySet()){
			splited = e.getKey().split(":");
			rname=splited[0];
			lname=splited[1];
			value=e.getValue();
			totalvaluel=token.get(rname);
			totalvaluer=token.get(lname);
			per1=calcPercent(value, totalvaluel);
			per2=calcPercent(value, totalvaluer);
			abs1=calcPercentLong(value,totalSum);
			output.println(rname+":"+lname+":"+Float.toString(per1)+":"+Float.toString(per2));			
			output.flush();
			outputl.println(rname+":"+lname+":"+Float.toString(per1));
			outputl.flush();
			outputr.println(rname+":"+lname+":"+Float.toString(per2));
			outputr.flush();
			left.put(rname+":"+lname, per1);
			right.put(rname+":"+lname, per2);
			combined.put(rname+":"+lname, (per1+per2)/2);
			absolutmap.put(rname+":"+lname, abs1);
		}
		try {
			writeToExcelAbsolut();
			writeToExcel(combined, sheetcombined);
			writeToExcel(left, sheetleft);
			writeToExcel(right, sheetright);
		} catch (RowsExceededException e1) {
			e1.printStackTrace();
		} catch (WriteException e1) {
			e1.printStackTrace();
		}
	}
	
	private void fillToken() throws NumberFormatException, IOException{
		String line="";
		String name;
		int value;
		String[] split;
		while ((line = tokenReader.readLine()) != null) {
			split=line.split(":");
			name=split[0];
			if (split.length==2 && !name.equalsIgnoreCase("Total count of tokens")){
				value = Integer.parseInt(split[1]);
				token.put(name, value);
			}
		}
		tokenReader.close();
	}
	
	private void fillFull() throws NumberFormatException, IOException{
		String line ="";
		String lname;
		String rname;
		int value;
		String[] split;
		while ((line = fullReader.readLine()) != null) {
			split=line.split(":");
			if (split.length==5){
				lname=split[0];
				rname=split[1];
				value=Integer.parseInt(split[4]);
				full.put(lname+":"+rname, value);
				totalSum=totalSum+value;
			}
		}
	}

	private float calcPercent(int incombined, int totalcount){
		float percentage = ((float)incombined)/(float)totalcount*100;
		return percentage;
	}
	
	private float calcPercentLong(int incombined, long totalcount){
		float percentage = ((float)incombined)/(float)totalcount*100;
		return percentage;
	}
	
	private void writeToExcel(HashMap<String, Float> toworkwith, WritableSheet toworksheet) throws RowsExceededException, WriteException{
		int excelline=0;
		Label label1;
		Label label2;
		Number number;
		ValueComparator bvc =  new ValueComparator(toworkwith);
		TreeMap<String,Float> sorted_map = new TreeMap<String,Float>(bvc);
		sorted_map.putAll(toworkwith);
		for (Entry<String, Float> e : sorted_map.entrySet()){
			label1 = new Label(0, excelline, e.getKey().split(":")[0]);
			toworksheet.addCell(label1);
			label2 = new Label(1, excelline, e.getKey().split(":")[1]);
			toworksheet.addCell(label2);
			number = new Number(2, excelline,e.getValue());
			toworksheet.addCell(number);
			excelline++;
			
		}
	}
	
	private void writeToExcelAbsolut() throws RowsExceededException, WriteException{
		int excelline=0;
		Label label1;
		Label label2;
		Number number;
		Formula formula;
		ValueComparatorInt bvc =  new ValueComparatorInt(full);
		TreeMap<String,Integer> sorted_map = new TreeMap<String,Integer>(bvc);
		sorted_map.putAll(full);
		for (Entry<String, Integer> e: sorted_map.entrySet()){
			label1 = new Label(0, excelline, e.getKey().split(":")[0]);
			absolut.addCell(label1);
			label2 = new Label(1, excelline, e.getKey().split(":")[1]);
			absolut.addCell(label2);
			number = new Number(2, excelline,e.getValue());
			absolut.addCell(number);
			formula = new Formula(3,excelline,"C"+Integer.toString(excelline+1)+"/"+"SUMME(C:C)*100");
			absolut.addCell(formula);
			excelline++;
		}



	}
}


