package statistics;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import jxl.Workbook;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class SingleHappeningFilter {
	private HashMap<String,HashMap<String, Integer>> allCombinations;
	private String output="single.xls";
	private int nextpackage;
	
	
	public SingleHappeningFilter(){
		allCombinations = new HashMap<String,HashMap<String, Integer>>();
		nextpackage=0;
	}
	
	public void addCase(BufferedReader caseread, String fullpathx) throws IOException{
		HashMap<String, Integer> temp = new HashMap<String, Integer>();
		String line="";
		String namel;
		String namer;
		String[] split;
		int count;
		while((line = caseread.readLine()) != null) {
			split=line.split(":");
			if (split.length==5){
				namel=split[0];
				namer=split[1];
				count=Integer.parseInt(split[4]);
				if (!namel.equalsIgnoreCase("Total count of tokens")){
					temp.put(namel+":"+namer,count);
				}
			}		
		}
		allCombinations.put(fullpathx,temp);
	}
	
	public void calculateResults(){
		String currentfullpah;	
		for (Entry<String, HashMap<String,Integer>> e : allCombinations.entrySet()){
			currentfullpah=e.getKey();
			HashMap<String,Integer> tempresults = new HashMap<String, Integer>();
			tempresults.putAll(e.getValue());
			for (Entry<String, HashMap<String,Integer>> x : allCombinations.entrySet()){
				if (!x.getKey().equals(currentfullpah)){
					for (Entry<String, Integer> v : x.getValue().entrySet()){
						if (currentfullpah.contains("ngdateierstellen1_gap1") && x.getKey().contains("ngsuchen1_gap1") && v.getKey().equals("Pool:Object")){
							int si = x.getValue().size();
							System.out.println("HA!");
						}
						if (tempresults.containsKey(v.getKey())){
							tempresults.remove(v.getKey());
						}
					}
				}
			}
			//Put it back for the next one
			try {
				writeToExcel(currentfullpah, tempresults);
			} catch (WriteException | IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	
	public void writeToExcel(String path, HashMap<String, Integer> res) throws IOException, RowsExceededException, WriteException{		
		WritableWorkbook wb = Workbook.createWorkbook(new File(path+output));
		WritableSheet sheet= wb.createSheet("Single", 0);;
		int excelline=0;
		Label label1;
		Label label2;
		Number number;
		Formula formula;
		ValueComparatorInt bvc =  new ValueComparatorInt(res);
		TreeMap<String,Integer> sorted_map = new TreeMap<String,Integer>(bvc);
		sorted_map.putAll(res);
		for (Entry<String, Integer> e: sorted_map.entrySet()){
			label1 = new Label(0, excelline, e.getKey().split(":")[0]);
			sheet.addCell(label1);
			label2 = new Label(1, excelline, e.getKey().split(":")[1]);
			sheet.addCell(label2);
			number = new Number(2, excelline,e.getValue());
			sheet.addCell(number);
			formula = new Formula(3,excelline,"C"+Integer.toString(excelline+1)+"/"+"SUMME(C:C)*100");
			sheet.addCell(formula);
			excelline++;
		}
		wb.write();
		wb.close();
	}
	
}
