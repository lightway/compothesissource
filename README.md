# README #

This is the repository with source code used for the Master Thesis "Entdecken von wiederkehrenden Zeichenketten in Szenarien bei Java Open Source Software zur Unterstützung der Lokalisation von Komponenten".
########
It has the following parts:
* AJTracer: Calltrace logger with AspectJ
* CGDistributedAnalyzer: Distributed Analyzer for tokenized analysis
* CalculateStats: Different further analysis for results of the Distributed Analyzer
* CallVerification: Needed as dependency for the Distributed Analyzer, mostly for database access.
* CombineTotalCount: To sum up the wordcount of a scenario from all three analyzed applications.
* DistributedAnalyzer-ServerScripts: Some shell scripts for Linux used to deploy and control the distributed analysing.
#####
Note: External libraries were removed, see the corresponding /libs/ folder for instructions which external .jar files are needed-

For more information please consult the corresponding Master Thesis.